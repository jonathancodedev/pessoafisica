package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class DominioTipoVO {
	
	private String tipo;
	private String codigo;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
