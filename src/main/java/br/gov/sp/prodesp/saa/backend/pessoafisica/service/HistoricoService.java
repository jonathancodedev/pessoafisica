package br.gov.sp.prodesp.saa.backend.pessoafisica.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.OcorrenciaPessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.OcorrenciaPessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.HistoricoPessoaFisicaVO;

@Service
public class HistoricoService {
	
	private static Logger logger = LoggerFactory.getLogger(PessoaFisicaService.class);

	@Autowired
	OcorrenciaPessoaFisicaRepository ocorrenciaRepository;

	public Page<HistoricoPessoaFisicaVO> buscaHistoricoPessoaFisica(String cpf, Pageable page) throws Exception {
		Page<OcorrenciaPessoaFisicaEntity> listaOcorrencia;
		Page<HistoricoPessoaFisicaVO> retorno = null;


		logger.info("BUSCA POR OCORRENCIA DE REGISTRO NO BANCO");
		try {
			listaOcorrencia = ocorrenciaRepository.findAllByCpf(cpf, page);
			
			if (listaOcorrencia.isEmpty()) {
				logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
				throw new ParametroNaoEncontradoException("NENHUM DADO ENCONTRADO COM O CPF: " + cpf);
			}
				retorno =  listaOcorrencia.map(ocorrencia -> {
					HistoricoPessoaFisicaVO ocorrenciaVO = new HistoricoPessoaFisicaVO();
					ocorrenciaVO.setTipo(ocorrencia.getStatus().getDominio());
					ocorrenciaVO.setDesc(ocorrencia.getDescricao());
					ocorrenciaVO.setDataHora(ocorrencia.getDataAtualizacao());
					ocorrenciaVO.setNomeUsuario(ocorrencia.getPessoa().getNome());
					ocorrenciaVO.setCpfUsuario(ocorrencia.getUsuarioAtualizacao().getPessoaFisica().getCpf());
					return ocorrenciaVO;
				});
				
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage(), e);

		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}

		return retorno;

	}

}