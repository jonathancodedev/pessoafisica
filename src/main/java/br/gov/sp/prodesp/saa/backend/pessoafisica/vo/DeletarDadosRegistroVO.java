package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class DeletarDadosRegistroVO {
	
	String cpf;
	String formacaoAcademica;
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getFormacaoAcademica() {
		return formacaoAcademica;
	}
	public void setFormacaoAcademica(String formacaoAcademica) {
		this.formacaoAcademica = formacaoAcademica;
	}
	
	
}
