package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.gov.sp.prodesp.saa.backend.pessoafisica.interfaces.PopulaDadosDeSessao;

@Entity
@Table(name = "TB_CDA_PESSOA_FISICA_RESPONSAVEL_TECNICO")
public class PessoaFisicaResponsavelTecnicoEntity extends UsuarioAtualizacao implements  PopulaDadosDeSessao{
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDENT")
    private Long id;

	@Column(name = "NUM_REG_PROFIS")
	private String numeroRegistroProfissional;

	@Column(name = "DATA_REG")
	private LocalDate dataRegistro;

	@Column(name = "VISTO")
	private String visto;
	
	@Column(name = "NUM_REG_PROFIS_NACIONAL")
	private String numeroRegProfissionalNacional;
	
	@Column(name = "NUM_REG_SECUND")
	private String numeroRegistroSecundarista;
	
    @JoinColumn(name = "ID_PESSOA", referencedColumnName = "ID_PESSOA")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private PessoaFisicaEntity pessoaFisica;

	@JoinColumn(name = "ID_DOM_FORMACAO_ACADEMICA", referencedColumnName = "IDENT")
	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	private DominioEntity formacaoAcademica;

	@JoinColumn(name = "ID_DOM_CONSELHO_PROF", referencedColumnName = "IDENT")
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private DominioEntity conselhoProfissional;

	@JoinColumn(name = "ID_UF_IBGE_REGISTRO", referencedColumnName = "IDENT")
	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	private UFEntity ufRegistro;

	@JoinColumn(name = "ID_DOM_TIPO_REGISTRO_PROF", referencedColumnName = "IDENT")
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	private DominioEntity tipoRegistroProfissional;

	public String getNumeroRegistroProfissional() {
		return numeroRegistroProfissional;
	}

	public void setNumeroRegistroProfissional(String numeroRegistroProfissional) {
		this.numeroRegistroProfissional = numeroRegistroProfissional;
	}

	public LocalDate getDataRegistro() {
		return dataRegistro;
	}

	public void setDataRegistro(LocalDate dataRegistro) {
		this.dataRegistro = dataRegistro;
	}

	public String getVisto() {
		return visto;
	}

	public void setVisto(String visto) {
		this.visto = visto;
	}

	public DominioEntity getConselhoProfissional() {
		return conselhoProfissional;
	}

	public void setConselhoProfissional(DominioEntity conselhoProfissional) {
		this.conselhoProfissional = conselhoProfissional;
	}

	public String getNumeroRegProfissionalNacional() {
		return numeroRegProfissionalNacional;
	}

	public void setNumeroRegProfissionalNacional(String numeroRegProfissionalNacional) {
		this.numeroRegProfissionalNacional = numeroRegProfissionalNacional;
	}

	public String getNumeroRegistroSecundarista() {
		return numeroRegistroSecundarista;
	}

	public void setNumeroRegistroSecundarista(String numeroRegistroSecundarista) {
		this.numeroRegistroSecundarista = numeroRegistroSecundarista;
	}

	public UFEntity getUfRegistro() {
		return ufRegistro;
	}

	public void setUfRegistro(UFEntity ufRegistro) {
		this.ufRegistro = ufRegistro;
	}

	public DominioEntity getTipoRegistroProfissional() {
		return tipoRegistroProfissional;
	}

	public void setTipoRegistroProfissional(DominioEntity tipoRegistroProfissional) {
		this.tipoRegistroProfissional = tipoRegistroProfissional;
	}

	public DominioEntity getFormacaoAcademica() {
		return formacaoAcademica;
	}

	public void setFormacaoAcademica(DominioEntity formacaoAcademica) {
		this.formacaoAcademica = formacaoAcademica;
	}

	public PessoaFisicaEntity getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisicaEntity pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public void populaDadosDeSessao(UsuarioEntity usuarioAtualizacao, DominioEntity status, LocalDate dataAtualizacao) {
		this.popular(usuarioAtualizacao, status, dataAtualizacao);
	}
	
	
	
}
