package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.EnderecoEntity;

@Repository
public interface EnderecoRepository extends PagingAndSortingRepository<EnderecoEntity, Long> {}
