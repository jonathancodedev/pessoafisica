package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class DominioVO {

	private String dominio;
    private String codigo;
    private DominioTipoVO tipo;
    
	public String getDominio() {
		return dominio;
	}
	public void setDominio(String dominio) {
		this.dominio = dominio;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public DominioTipoVO getTipo() {
		return tipo;
	}
	public void setTipo(DominioTipoVO tipo) {
		this.tipo = tipo;
	}
	
    
}
