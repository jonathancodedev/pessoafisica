package br.gov.sp.prodesp.saa.backend.pessoafisica.exception;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public ServiceException(String msg) {
		super(msg);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
