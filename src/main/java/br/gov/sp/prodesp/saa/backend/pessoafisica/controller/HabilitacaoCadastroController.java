package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.DadosJaCadastradosException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroInvalidoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.service.HabilitacaoCadastroService;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.DeletarDadosHabilitacaoVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaHabilitacaoCredenciamentoVO;

@RestController
@RequestMapping("/habilitacao")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class HabilitacaoCadastroController {
	
	private static final String HEADER_MESSAGE = "mensagem";

	@Autowired
	HabilitacaoCadastroService habilitacaoCadastroService;

	@GetMapping("/{cpf}")
	public ResponseEntity<Page<PessoaFisicaHabilitacaoCredenciamentoVO>> buscaHabilitacaoCadastro(
			@PathVariable(required = true) String cpf, @PageableDefault(size = 20) Pageable pageable) {

		ResponseEntity<Page<PessoaFisicaHabilitacaoCredenciamentoVO>> response = null;

		try {
			response = ResponseEntity.ok(habilitacaoCadastroService.buscaHabilitacaoCadastro(cpf, pageable));
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@PostMapping
	public ResponseEntity<PessoaFisicaHabilitacaoCredenciamentoVO> salvaHabilitacaoCadastro(
			@RequestBody PessoaFisicaHabilitacaoCredenciamentoVO habilitacao) {

		ResponseEntity<PessoaFisicaHabilitacaoCredenciamentoVO> response = null;

		try {
			PessoaFisicaHabilitacaoCredenciamentoVO habilitacaoVO = habilitacaoCadastroService.incluirHabilitacaoCadastro(habilitacao);
			response = ResponseEntity.ok(habilitacaoVO);
			
		} catch (ParametroInvalidoException e) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (DadosJaCadastradosException e) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@PutMapping
	public ResponseEntity<PessoaFisicaHabilitacaoCredenciamentoVO> alterarHabilitacaoCadastro(
			@RequestBody PessoaFisicaHabilitacaoCredenciamentoVO habilitacao) {
		
		ResponseEntity<PessoaFisicaHabilitacaoCredenciamentoVO> response = null;

		try {
			PessoaFisicaHabilitacaoCredenciamentoVO habilitacaoVO = this.habilitacaoCadastroService.alterarHabilitacaoCadastro(habilitacao);
			response = ResponseEntity.ok(habilitacaoVO);

		} catch (ParametroInvalidoException e) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@DeleteMapping
	public ResponseEntity<Object> deletarHabilitacaoCadastro(
			@RequestBody DeletarDadosHabilitacaoVO pessoaResponsavel) {

		ResponseEntity<Object> response = null;

		try {
			habilitacaoCadastroService.deletarHabilitacaoCadastro(pessoaResponsavel);
			response = ResponseEntity.ok().build();
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
}
