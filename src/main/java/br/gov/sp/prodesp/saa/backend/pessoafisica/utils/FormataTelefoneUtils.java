package br.gov.sp.prodesp.saa.backend.pessoafisica.utils;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ContatoTelefoneEntity;

public class FormataTelefoneUtils {
	
	private FormataTelefoneUtils() {}
	
	public static String getTelefoneFormatado(ContatoTelefoneEntity contato) {
        StringBuilder builder = new StringBuilder();
        String result;
        
        if (contato != null) {
        
        	if(contato.getDdd() !=null){
        		builder.append("(");
        		builder.append(contato.getDdd());
        		builder.append(") ");
        	}
        	
        	if(contato.getTelefone() !=null)
        		builder.append(contato.getTelefone().replaceAll("^(\\d{4})(\\d{4})$", "$1-$2"));
        	
        	if (contato.getRamal() != null) {
        		builder.append(" ");
        		builder.append(contato.getRamal());
        	}        
        	
        	result = builder.toString();
        
        } else { result = null; } 
        

        return result;
    }
	
}
