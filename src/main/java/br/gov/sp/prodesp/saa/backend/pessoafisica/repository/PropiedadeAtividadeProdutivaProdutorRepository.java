package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PropiedadeAtividadeProdutivaProdutorEntity;

@Repository
public interface PropiedadeAtividadeProdutivaProdutorRepository extends 
PagingAndSortingRepository<PropiedadeAtividadeProdutivaProdutorEntity, Long> {
	
	@Query("SELECT ap FROM PropiedadeAtividadeProdutivaProdutorEntity ap WHERE ap.produtor.id = ?1")
	Page<PropiedadeAtividadeProdutivaProdutorEntity> findAllByPessoa(Long id, Pageable page);

}
