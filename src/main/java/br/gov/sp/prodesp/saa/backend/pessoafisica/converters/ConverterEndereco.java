package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.EnderecoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.CheckObjectUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.EnderecoVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.MunicipioVO;

public class ConverterEndereco {
	
	public EnderecoEntity converterParaEnderecoEntity(EnderecoEntity entity, EnderecoVO vo){
		entity.setCep(vo.getCep());
		entity.setBairro(vo.getBairro());
		entity.setLogradouro(vo.getLogradouro());
		entity.setNumero(vo.getNumero());
		entity.setCaixaPostal(CheckObjectUtils.checkString(vo.getCaixaPostal()));
		entity.setComplemento(CheckObjectUtils.checkString(vo.getComplemento()));
		
		return entity;
	}

	public EnderecoVO converterParaEnderecoVO(EnderecoEntity  entity, EnderecoVO vo) {
		vo.setCep(entity.getCep());
		vo.setBairro(entity.getBairro());
		vo.setLogradouro(entity.getLogradouro());
		vo.setNumero(entity.getNumero());
		vo.setMunicipio(ConverterMunicipio.converterParaVO(entity.getMunicipio(), new MunicipioVO()));
		vo.setCaixaPostal(CheckObjectUtils.checkString(entity.getCaixaPostal()));
		vo.setComplemento(CheckObjectUtils.checkString(entity.getComplemento()));
		
		return vo;
	} 
}
