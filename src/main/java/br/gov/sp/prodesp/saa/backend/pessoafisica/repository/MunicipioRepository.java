package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.MunicipioEntity;

public interface MunicipioRepository extends PagingAndSortingRepository<MunicipioEntity, Long> {
	
	@Query("SELECT ha FROM MunicipioEntity ha WHERE ha.nome = ?1")
	MunicipioEntity findByNome(String nome);
}
