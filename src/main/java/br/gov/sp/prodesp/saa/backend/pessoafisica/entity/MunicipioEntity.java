package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_MUNICIPIO_IBGE")
public class MunicipioEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	public Long getId() {
		return id;
	}
	
	@Column(name = "NOME_MUN")
	private String nome;
	
	@JoinColumn(name = "ID_UF_IBGE", referencedColumnName = "IDENT")
    @ManyToOne(optional = false)
    private UFEntity uf;

	public String getNome() {
		return nome;
	}

	public UFEntity getUf() {
		return uf;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setUf(UFEntity uf) {
		this.uf = uf;
	}
	
	


}
