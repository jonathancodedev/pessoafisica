package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

import java.time.LocalDate;

public class PessoaFisicaResponsavelTecnicoVO {
	
	String formacao;
	String conselhoProfissional;
	String nRegistroEstadual;
	LocalDate dataRegistro;
	String tipoRegistro;
	String ufRegistro;
	String cpf;
	String nVistoInscricaoSecudanria;
	String nRegistroNacional;
	
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public LocalDate getDataRegistro() {
		return dataRegistro;
	}
	public void setDataRegistro(LocalDate dataRegistro) {
		this.dataRegistro = dataRegistro;
	}
	public String getnVistoInscricaoSecudanria() {
		return nVistoInscricaoSecudanria;
	}
	public void setnVistoInscricaoSecudanria(String nVistoInscricaoSecudanria) {
		this.nVistoInscricaoSecudanria = nVistoInscricaoSecudanria;
	}
	public String getnRegistroNacional() {
		return nRegistroNacional;
	}
	public void setnRegistroNacional(String nRegistroNacional) {
		this.nRegistroNacional = nRegistroNacional;
	}
	public String getnRegistroEstadual() {
		return nRegistroEstadual;
	}
	public void setnRegistroEstadual(String nRegistroEstadual) {
		this.nRegistroEstadual = nRegistroEstadual;
	}
	public String getUfRegistro() {
		return ufRegistro;
	}
	public void setUfRegistro(String ufRegistro) {
		this.ufRegistro = ufRegistro;
	}
	public String getConselhoProfissional() {
		return conselhoProfissional;
	}
	public void setConselhoProfissional(String conselhoProfissional) {
		this.conselhoProfissional = conselhoProfissional;
	}
	public String getFormacao() {
		return formacao;
	}
	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}
	public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

}
