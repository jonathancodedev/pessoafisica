package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_DOMINIO")
public class DominioEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	@Basic(optional = false)
	@Column(name = "DOMINIO")
	private String dominio;

    @Column(name = "COD_DOMINIO")
    private String codigo;
    
    @JoinColumn(name = "ID_TIPO", referencedColumnName = "IDENT")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DominioTipoEntity tipo;


	public Long getId() {
		return id;
	}

	public String getDominio() {
		return dominio;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	public DominioTipoEntity getTipo() {
		return tipo;
	}

	public void setTipo(DominioTipoEntity tipo) {
		this.tipo = tipo;
	}
	

}
