package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class PaisVO {

	private String nome;
	private String referencia;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
}
