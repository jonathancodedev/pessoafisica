package br.gov.sp.prodesp.saa.backend.pessoafisica.exception;

public class DadosJaCadastradosException extends ServiceException{

	private static final long serialVersionUID = 1L;

	public DadosJaCadastradosException(String msg) {
		super(msg);
	}
	public DadosJaCadastradosException(String msg, Throwable tr) {
		super(msg, tr);
	}
	
}
