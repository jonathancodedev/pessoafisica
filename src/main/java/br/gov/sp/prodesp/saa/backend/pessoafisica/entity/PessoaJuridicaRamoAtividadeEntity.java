package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_PESSOA_JURIDICA_RAMO_ATIVIDADE")
public class PessoaJuridicaRamoAtividadeEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	
	@JoinColumn(name = "ID_PESSOA_JURIDICA", referencedColumnName = "ID_PESSOA", nullable = false)
	@ManyToOne(optional = false)
	private PessoaJuridicaEntity pessoaJuridica;

	@JoinColumn(name = "ID_DOM_RAMO_ATIVIDADE", referencedColumnName = "IDENT", nullable = false)
	@ManyToOne(optional = false)
	private DominioEntity ramoAtividade;
	
	public Long getId() {
		return id;
	}

	public PessoaJuridicaEntity getPessoaJuridica() {
		return pessoaJuridica;
	}

	public DominioEntity getRamoAtividade() {
		return ramoAtividade;
	}
	
	
}
