package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UFEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.UFVO;

public class ConverterUF {

	public static UFEntity converterParaEntidade(UFEntity entity, UFVO vo){
		entity.setNome(vo.getNome());
		entity.setCodigo(vo.getCodigo());
		
		return entity;
	}

	public static UFVO converterParaVO(UFEntity  entity, UFVO vo) {
		vo.setNome(entity.getNome());
		vo.setCodigo(entity.getCodigo());
		
		return vo;
	} 
}
