package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ContatoEletronicoEntity;

@Repository
public interface ContatoEletronicoRepository extends 
PagingAndSortingRepository<ContatoEletronicoEntity, Long> {
	
	@Query("SELECT ce FROM ContatoEletronicoEntity ce WHERE ce.descricao = ?1")
	Optional<ContatoEletronicoEntity> findByDescricao(String desc);
	
}