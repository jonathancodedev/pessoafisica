package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_PROPRIEDADE_ATIVIDADE_PRODUTIVA_PRODUTOR")
public class PropiedadeAtividadeProdutivaProdutorEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	@JoinColumn(name = "ID_PROPRIEDADE_AP", referencedColumnName = "IDENT", nullable = true)
	@OneToOne(fetch = FetchType.LAZY)
	private AtividadeProdutivaEntity atividadeProdutiva;
	
    @ManyToOne
    @JoinColumn(name = "ID_PESSOA", referencedColumnName = "IDENT", nullable = false)
    private PessoaEntity produtor;

    @ManyToOne
    @JoinColumn(name = "ID_DOM_TIPO_PRODUTOR", nullable = false)
    private DominioEntity tipoProdutor;

    @ManyToOne
    @JoinColumn(name = "ID_DOM_STATUS", nullable = false)
    private DominioEntity status;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AtividadeProdutivaEntity getAtividadeProdutiva() {
		return atividadeProdutiva;
	}

	public void setAtividadeProdutiva(AtividadeProdutivaEntity atividadeProdutiva) {
		this.atividadeProdutiva = atividadeProdutiva;
	}

	public DominioEntity getTipoProdutor() {
		return tipoProdutor;
	}

	public PessoaEntity getProdutor() {
		return produtor;
	}

	public DominioEntity getStatus() {
		return status;
	}

	public void setStatus(DominioEntity status) {
		this.status = status;
	}
	
	
	
	
}
