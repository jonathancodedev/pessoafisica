package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_APIARIO_RESPONSAVEL")
public class ApiarioResponsavelEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PESSOA", referencedColumnName = "IDENT", nullable = false)
	private PessoaEntity pessoa;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PJ_RAMO_ATIVIDADE", referencedColumnName = "IDENT", nullable = true)
	private PessoaJuridicaRamoAtividadeEntity ramoAtividade;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_APIARIO", referencedColumnName = "IDENT", nullable = false)
	private ApiarioEntity apiario;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_DOM_TIPO_PRODUTOR", referencedColumnName = "IDENT", nullable = false)
	private DominioEntity tipoProdutor;
	
	public Long getId() {
		return id;
	}

	public PessoaEntity getPessoa() {
		return pessoa;
	}

	public PessoaJuridicaRamoAtividadeEntity getRamoAtividade() {
		return ramoAtividade;
	}

	public ApiarioEntity getApiario() {
		return apiario;
	}

	public DominioEntity getTipoProdutor() {
		return tipoProdutor;
	}
	
	
}
