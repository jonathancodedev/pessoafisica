package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UFEntity;

public interface UFRepository extends PagingAndSortingRepository<UFEntity, Long> {
	
	@Query("SELECT uf FROM UFEntity uf WHERE uf.codigo = ?1")
	UFEntity findByCodUF(String estadoCivil);

}
