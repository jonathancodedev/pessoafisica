package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class VinculoAtividadeProdutivaVO {
	
	Long codigo;
	String descAtividadeProdutiva;
	String vertente;
	String nomePropiedade;
	String nomeMunicipio;
	String vinculo;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getDescAtividadeProdutiva() {
		return descAtividadeProdutiva;
	}
	public void setDescAtividadeProdutiva(String descAtividadeProdutiva) {
		this.descAtividadeProdutiva = descAtividadeProdutiva;
	}
	public String getVertente() {
		return vertente;
	}
	public void setVertente(String vertente) {
		this.vertente = vertente;
	}
	public String getNomePropiedade() {
		return nomePropiedade;
	}
	public void setNomePropiedade(String nomePropiedade) {
		this.nomePropiedade = nomePropiedade;
	}
	public String getNomeMunicipio() {
		return nomeMunicipio;
	}
	public void setNomeMunicipio(String nomeMunicipio) {
		this.nomeMunicipio = nomeMunicipio;
	}
	public String getVinculo() {
		return vinculo;
	}
	public void setVinculo(String vinculo) {
		this.vinculo = vinculo;
	}
	
//	private Long codigo;
//	private Long idAtividadeProdutiva;
//	private Long numAtividadeProdutiva;
//	private String descAtividadeProdutiva;
//	private String vertente;
//	private String nomePropriedade;
//	private String nomeMunicipio;
//	private String vinculo;
//	private String codVertente;
//	private boolean isRt;
//	private String codVinculo;
//	private Long idPessoa;
//	
	
}
