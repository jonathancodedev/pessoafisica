package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_PESSOA_FISICA")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class PessoaFisicaEntity extends PessoaEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Basic(optional = false)
    @Column(name = "COD_CPF", nullable = false)
    private String cpf;

    @Basic(optional = false)
    @Column(name = "COD_RG_RNE", nullable = false)
    private String rgRne;
    
    @Basic(optional = false)
    @Column(name = "DATA_EMIS_RG", nullable = false)
    private LocalDate dataEmissaoRG;
    
    @Basic(optional = true)
    @Column(name = "DATA_NASCIM", nullable = true)
    private LocalDate dataNascimento;
    
    @Column(name = "NOME_MAE", nullable = true)
    private String nomeMae;

    @Column(name = "NOME_PAI", nullable = true)
    private String nomePai;
    
    @Column(name = "FLAG_RECEBE_SMS", nullable = true)
    private Character flagRecebeSms;

    @Column(name = "FLAG_RECEBE_EMAIL", nullable = true)
    private Character flagRecebeEmail;

    @Column(name = "DESCR_OBSERV", nullable = true)
    private String observacao;
    
    @Column(name = "FLAG_CAIXA_POSTAL_CORR", nullable = true)
    private Character flagCaixaPostalCorr;
    
    @Column(name = "FLAG_REG_TEC", nullable = true)
    private Character flagRegistroTecnico;

    
    @Column(name = "ORGAO_EMISSOR", nullable = true)
    private String orgaoEmissor;

    @Column(name = "FLAG_MESMO_END_RES", nullable = true)
    private Character flagMesmoEndRes;
    
    
    // ----------------------- RELACIONAMENTOS IDA ---------------------------
    
    @JoinColumn(name = "ID_UF_IBGE_ORGAO_EMISSOR_RG", referencedColumnName = "IDENT", nullable = true)
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private UFEntity ufOrgaoEmissorRg;
    
    @JoinColumn(name = "ID_MUNICIPIO_IBGE_NASCIMENTO", referencedColumnName = "IDENT", nullable = true)
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private MunicipioEntity municipioNaturalidade;
    
    @JoinColumn(name = "ID_ENDERECO_RESIDENCIAL", referencedColumnName = "IDENT", nullable = true)
    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private EnderecoEntity enderecoResidencial;
    
    @JoinColumn(name = "ID_ENDERECO_CORRESPONDENCIA", referencedColumnName = "IDENT", nullable = true)
    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private EnderecoEntity enderecoCorrespondencia;
    
    @JoinColumn(name = "ID_DOM_SEXO", referencedColumnName = "IDENT", nullable = true)
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private DominioEntity sexo;
    
    @JoinColumn(name = "ID_DOM_ESTADO_CIVIL", referencedColumnName = "IDENT", nullable = true)
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private DominioEntity estadoCivil;
    
    @JoinColumn(name = "ID_PAIS_NACIONALIDADE", referencedColumnName = "IDENT", nullable = true)
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private PaisEntity nacionalidade;
    
    @JoinColumn(name = "ID_CONTATO_TELEFONE_RESIDENCIAL", referencedColumnName = "IDENT", nullable = true)
    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private ContatoTelefoneEntity contatoTelefoneResidencial;
    
    @JoinColumn(name = "ID_CONTATO_TELEFONE_MOVEL", referencedColumnName = "IDENT", nullable = true)
    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private ContatoTelefoneEntity contatoTelefoneMovel;
    
    @JoinColumn(name = "ID_CONTATO_TELEFONE_COMERCIAL", referencedColumnName = "IDENT", nullable = true)
    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private ContatoTelefoneEntity contatoTelefoneComercial;
    
    @JoinColumn(name = "ID_CONTATO_ELETRONICO_EMAIL", referencedColumnName = "IDENT", nullable = true)
    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private ContatoEletronicoEntity contatoEmail;
    
    @JoinColumn(name = "ID_DOM_ORGAO_EMISSOR_RG", referencedColumnName = "IDENT", nullable = true)
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private DominioEntity orgaoEmissorRg;
    
	public String getCpf() {
		return cpf;
	}
	
	public String getRgRne() {
		return rgRne;
	}

	public MunicipioEntity getMunicipioNaturalidade() {
		return municipioNaturalidade;
	}

	public EnderecoEntity getEnderecoResidencial() {
		return enderecoResidencial;
	}

	public LocalDate getDataEmissaoRG() {
		return dataEmissaoRG;
	}
	
	public String getNomeMae() {
		return nomeMae;
	}

	public String getNomePai() {
		return nomePai;
	}

	public UFEntity getUfOrgaoEmissorRg() {
		return ufOrgaoEmissorRg;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public DominioEntity getSexo() {
		return sexo;
	}

	public DominioEntity getEstadoCivil() {
		return estadoCivil;
	}

	public PaisEntity getNacionalidade() {
		return nacionalidade;
	}

	public EnderecoEntity getEnderecoCorrespondencia() {
		return enderecoCorrespondencia;
	}

	public ContatoTelefoneEntity getContatoTelefoneResidencial() {
		return contatoTelefoneResidencial;
	}

	public ContatoTelefoneEntity getContatoTelefoneMovel() {
		return contatoTelefoneMovel;
	}

	public ContatoTelefoneEntity getContatoTelefoneComercial() {
		return contatoTelefoneComercial;
	}

	public Character getFlagRecebeSms() {
		return flagRecebeSms;
	}

	public Character getFlagRecebeEmail() {
		return flagRecebeEmail;
	}

	public ContatoEletronicoEntity getContatoEmail() {
		return contatoEmail;
	}

	public String getObservacao() {
		return observacao;
	}

	public Character getFlagCaixaPostalCorr() {
		return flagCaixaPostalCorr;
	}

	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}

	public Character getFlagRegistroTecnico() {
		return flagRegistroTecnico;
	}

	public DominioEntity getOrgaoEmissorRg() {
		return orgaoEmissorRg;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setRgRne(String rgRne) {
		this.rgRne = rgRne;
	}

	public void setDataEmissaoRG(LocalDate dataEmissaoRG) {
		this.dataEmissaoRG = dataEmissaoRG;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public void setFlagRecebeSms(Character flagRecebeSms) {
		this.flagRecebeSms = flagRecebeSms;
	}

	public void setFlagRecebeEmail(Character flagRecebeEmail) {
		this.flagRecebeEmail = flagRecebeEmail;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setFlagCaixaPostalCorr(Character flagCaixaPostalCorr) {
		this.flagCaixaPostalCorr = flagCaixaPostalCorr;
	}

	public void setFlagRegistroTecnico(Character flagRegistroTecnico) {
		this.flagRegistroTecnico = flagRegistroTecnico;
	}

	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}

	public void setUfOrgaoEmissorRg(UFEntity ufOrgaoEmissorRg) {
		this.ufOrgaoEmissorRg = ufOrgaoEmissorRg;
	}

	public void setMunicipioNaturalidade(MunicipioEntity municipioNaturalidade) {
		this.municipioNaturalidade = municipioNaturalidade;
	}

	public void setSexo(DominioEntity sexo) {
		this.sexo = sexo;
	}

	public void setEstadoCivil(DominioEntity estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public void setNacionalidade(PaisEntity nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public void setOrgaoEmissorRg(DominioEntity orgaoEmissorRg) {
		this.orgaoEmissorRg = orgaoEmissorRg;
	}

	public void setEnderecoResidencial(EnderecoEntity enderecoResidencial) {
		this.enderecoResidencial = enderecoResidencial;
	}

	public void setEnderecoCorrespondencia(EnderecoEntity enderecoCorrespondencia) {
		this.enderecoCorrespondencia = enderecoCorrespondencia;
	}

	public void setContatoTelefoneResidencial(ContatoTelefoneEntity contatoTelefoneResidencial) {
		this.contatoTelefoneResidencial = contatoTelefoneResidencial;
	}

	public void setContatoTelefoneMovel(ContatoTelefoneEntity contatoTelefoneMovel) {
		this.contatoTelefoneMovel = contatoTelefoneMovel;
	}

	public void setContatoTelefoneComercial(ContatoTelefoneEntity contatoTelefoneComercial) {
		this.contatoTelefoneComercial = contatoTelefoneComercial;
	}

	public void setContatoEmail(ContatoEletronicoEntity contatoEmail) {
		this.contatoEmail = contatoEmail;
	}

	public Character getFlagMesmoEndRes() {
		return flagMesmoEndRes;
	}

	public void setFlagMesmoEndRes(Character flagMesmoEndRes) {
		this.flagMesmoEndRes = flagMesmoEndRes;
	}

	
	
}
