package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaHabilitacaoCredenciamentoEntity;

public interface PessoaFisicaHabilitacaoCredenciamentoRepository
		extends PagingAndSortingRepository<PessoaFisicaHabilitacaoCredenciamentoEntity, Long> {

	@Query("SELECT ha FROM PessoaFisicaHabilitacaoCredenciamentoEntity ha WHERE ha.pessoaFisica.id = ?1")
	Page<PessoaFisicaHabilitacaoCredenciamentoEntity> findByIdPessoa(Long id, Pageable pageable);

	@Query("SELECT ha FROM PessoaFisicaHabilitacaoCredenciamentoEntity ha WHERE ha.pessoaFisica.cpf = ?1")
	Page<PessoaFisicaHabilitacaoCredenciamentoEntity> findByCpf(String cpf, Pageable pageable);

	@Query("SELECT ha FROM PessoaFisicaHabilitacaoCredenciamentoEntity ha WHERE ha.pessoaFisica.cpf = ?1 AND ha.habilitacaoCredenciamento.dominio = ?2")
	Optional<PessoaFisicaHabilitacaoCredenciamentoEntity> findByCpfAndHabilitacao(String cpf, String dominio);

	@Transactional
	@Modifying
	@Query("DELETE FROM PessoaFisicaHabilitacaoCredenciamentoEntity ha WHERE ha.pessoaFisica.cpf = ?1")
	void deleteAllByCpf(String cpf);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM PessoaFisicaHabilitacaoCredenciamentoEntity ha WHERE ha.id = ?1")
	void deleteById(Long id);
	
}
