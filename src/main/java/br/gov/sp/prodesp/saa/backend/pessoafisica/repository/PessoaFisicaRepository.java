package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaEntity;

@Repository
public interface PessoaFisicaRepository extends PagingAndSortingRepository<PessoaFisicaEntity, Long> {
	
	Optional<PessoaFisicaEntity> findByCpf(String cpf);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM PessoaFisicaEntity ha WHERE ha.id = ?1")
	void deleteById(Long id);
 
}