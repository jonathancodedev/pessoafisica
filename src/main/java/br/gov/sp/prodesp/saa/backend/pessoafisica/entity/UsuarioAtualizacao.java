package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class UsuarioAtualizacao {
	
    @Column(name = "DATA_ATUALIZ", nullable = true)
    private LocalDate dataAtualizacao;

    @JoinColumn(name = "ID_USUAR_ATUALIZ", referencedColumnName = "IDENT")
    @ManyToOne(optional = true,  cascade = CascadeType.ALL)
    private UsuarioEntity usuarioAtualizacao;
    
    @JoinColumn(name = "ID_DOM_STATUS", referencedColumnName = "IDENT")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private DominioEntity status;

	public void popular(UsuarioEntity usuarioAtualizacao, DominioEntity status, LocalDate dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
		this.status = status;
		this.usuarioAtualizacao = usuarioAtualizacao;
	}

	public LocalDate getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDate dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public UsuarioEntity getUsuarioAtualizacao() {
		return usuarioAtualizacao;
	}

	public void setUsuarioAtualizacao(UsuarioEntity usuarioAtualizacao) {
		this.usuarioAtualizacao = usuarioAtualizacao;
	}

	public DominioEntity getStatus() {
		return status;
	}

	public void setStatus(DominioEntity status) {
		this.status = status;
	}
	
	
    
}
