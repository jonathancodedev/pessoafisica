package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.service.HistoricoService;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.HistoricoPessoaFisicaVO;

@RestController
@RequestMapping("/historico")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class HistoricoController {
	
	private static final String HEADER_MESSAGE = "mensagem";

	@Autowired
	HistoricoService historicoService;
	
	@GetMapping("/{cpf}")
	public ResponseEntity<Page<HistoricoPessoaFisicaVO>> buscaGeral(
			@PathVariable String cpf,
			@PageableDefault(size = 20) Pageable pageable) {

		ResponseEntity<Page<HistoricoPessoaFisicaVO>> response = null;

		try {
			response = ResponseEntity.ok(historicoService.buscaHistoricoPessoaFisica(cpf, pageable));
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}

}
