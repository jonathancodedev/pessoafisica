package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class UnidadeAdministrativaVO {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
}
