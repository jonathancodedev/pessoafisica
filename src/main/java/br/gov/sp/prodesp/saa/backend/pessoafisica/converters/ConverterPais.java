package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PaisEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PaisVO;

public class ConverterPais {
	
	public static PaisEntity converterParaEntidade(PaisEntity entity, PaisVO vo){
		entity.setNome(vo.getNome());
		entity.setReferencia(vo.getReferencia());
		return entity;
	}

	public static PaisVO converterParaVO(PaisEntity  entity, PaisVO vo) {
		vo.setNome(entity.getNome());
		vo.setReferencia(entity.getReferencia());
		return vo;
	} 

}
