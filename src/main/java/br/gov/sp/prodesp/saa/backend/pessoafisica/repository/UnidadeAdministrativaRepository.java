package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UnidadeAdministrativaEntity;

public interface UnidadeAdministrativaRepository extends 
PagingAndSortingRepository<UnidadeAdministrativaEntity, Long> {

	@Query("SELECT ua FROM UnidadeAdministrativaEntity ua WHERE ua.nome = ?1")
	UnidadeAdministrativaEntity findByNome(String nome);

}
