package br.gov.sp.prodesp.saa.backend.pessoafisica.service;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaResponsavelTecnicoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UsuarioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.DadosJaCadastradosException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.DominioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaResponsavelTecnicoRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.UFRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.UsuarioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.VerificaCPFUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.DeletarDadosRegistroVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaResponsavelTecnicoVO;

@Service
public class RegistroProfissionalService {

	@Autowired
	PessoaFisicaResponsavelTecnicoRepository pessoaResponsavelRepository;

	@Autowired
	PessoaFisicaRepository pfRepository;

	@Autowired
	DominioRepository dominioRepository;

	@Autowired
	UFRepository ufRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	private static Logger logger = LoggerFactory.getLogger(RegistroProfissionalService.class);

	public Page<PessoaFisicaResponsavelTecnicoVO> buscaRegistroConselhoProfisssional(String cpf, Pageable page)
			throws Exception {

		Page<PessoaFisicaResponsavelTecnicoVO> retorno = null;

		try {
			logger.info("BUSCA PESSOA FISICA RESPONSAVEL TECNICO POR CPF NO BANCO");
			Page<PessoaFisicaResponsavelTecnicoEntity> listaPessoaResponsavel = pessoaResponsavelRepository
					.findByCpfPessoa(cpf, page);
			if (listaPessoaResponsavel.isEmpty()) {
				throw new ParametroNaoEncontradoException("PARAMETRO NÃO ENCONTRADO");
			}
			
			retorno = listaPessoaResponsavel.map(pessoaFisica -> {
				PessoaFisicaResponsavelTecnicoVO pessoaResponsavelVO = new PessoaFisicaResponsavelTecnicoVO();
				pessoaResponsavelVO = converterParaVO(pessoaFisica, pessoaResponsavelVO);
				return pessoaResponsavelVO;
			});

		} catch (ParametroNaoEncontradoException e) {
			logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
			throw new ParametroNaoEncontradoException(e.getMessage());
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}

		return retorno;
	}

	public PessoaFisicaResponsavelTecnicoVO alteraRegistroConselhoProfissional(
			PessoaFisicaResponsavelTecnicoVO pessoaResponsavelVO) throws Exception {

		VerificaCPFUtils.verificaCPF(pessoaResponsavelVO.getCpf(), logger);
		
		try {
			logger.info("BUSCA PESSOA FISICA RESPONSAVEL TECNICO POR CPF NO BANCO");
			PessoaFisicaResponsavelTecnicoEntity pessoaResponsavelEntity = pessoaResponsavelRepository
					.findByCpfPessoaAndFormacao(pessoaResponsavelVO.getCpf(), pessoaResponsavelVO.getFormacao())
					.orElseThrow(() -> new ParametroNaoEncontradoException("PARAMETRO NÃO ENCONTRADO"));
	
			pessoaResponsavelEntity = converterParaEntidade(pessoaResponsavelEntity, pessoaResponsavelVO);
			
			logger.info("SALVANDO ATUALIZACAO DE DADOS DE REGISTRO PROFISSIONAL NO BANCO");
			pessoaResponsavelRepository.save(pessoaResponsavelEntity);
	
			return pessoaResponsavelVO;
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
			throw new ParametroNaoEncontradoException(e.getMessage());
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	public PessoaFisicaResponsavelTecnicoVO incluirRegistroConselhoProfissional(
			PessoaFisicaResponsavelTecnicoVO pessoaResponsavelVO) throws Exception {

		VerificaCPFUtils.verificaCPF(pessoaResponsavelVO.getCpf(), logger);
		logger.info("VERIFICANDO EXISTENCIA DE REGISTRO PROFISSIONAL NO BANCO");
		if (pessoaResponsavelRepository
				.findByCpfPessoaAndFormacao(pessoaResponsavelVO.getCpf(), pessoaResponsavelVO.getFormacao())
				.isPresent()) {
			logger.error("DADOS JÁ EXISTENTES NO BANCO");
			throw new DadosJaCadastradosException("Registro já existe no banco");
		}
		
		try {
	
			PessoaFisicaResponsavelTecnicoEntity pessoaResponsavelEntity = new PessoaFisicaResponsavelTecnicoEntity();
	
			UsuarioEntity usuario = usuarioRepository.findById(2L).get();
			DominioEntity status = dominioRepository.findById(101L).get();
			LocalDate now = LocalDate.now();
			pessoaResponsavelEntity = converterParaEntidade(new PessoaFisicaResponsavelTecnicoEntity(),
					pessoaResponsavelVO);
			pessoaResponsavelEntity.populaDadosDeSessao(usuario, status, now);
			
			logger.info("SALVANDO REGISTRO PROFISSIONAL NO BANCO");
			pessoaResponsavelRepository.save(pessoaResponsavelEntity);
	
			return pessoaResponsavelVO;
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	public void deletarRegistroConselhoProfissional(DeletarDadosRegistroVO pessoa) throws Exception {
		
		try {
			PessoaFisicaResponsavelTecnicoEntity entity = pessoaResponsavelRepository
					.findByCpfPessoaAndFormacao(pessoa.getCpf(), pessoa.getFormacaoAcademica())
					.orElseThrow(() -> new ParametroNaoEncontradoException("PARAMETRO NÃO ENCONTRADO"));
			;
			
			logger.info("DELETANDO REGISTRO PROFISSIONAL NO BANCO");
			pessoaResponsavelRepository.deleteById(entity.getId());
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
			throw new ParametroNaoEncontradoException(e.getMessage());
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	
	public void deletarTodosRegistroConselhoProfissional(String cpf, PageRequest page) throws Exception {
		
		try {
			logger.info("DELETANDO DADOS DE REGISTRO TECNICO DO BANCO");
			if (!pessoaResponsavelRepository.findByCpfPessoa(cpf, page).isEmpty()) {
				pessoaResponsavelRepository.deleteCpfPessoa(cpf);
			}
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	
	public PessoaFisicaResponsavelTecnicoEntity converterParaEntidade(PessoaFisicaResponsavelTecnicoEntity entity,
			PessoaFisicaResponsavelTecnicoVO vo) {

		entity.setDataRegistro(vo.getDataRegistro());
		entity.setNumeroRegistroProfissional(vo.getnRegistroEstadual());
		entity.setNumeroRegProfissionalNacional(vo.getnRegistroNacional());
		entity.setNumeroRegistroSecundarista(vo.getnVistoInscricaoSecudanria());

		entity.setPessoaFisica(pfRepository.findByCpf(vo.getCpf()).get());
		entity.setConselhoProfissional(dominioRepository.findByDominio(vo.getConselhoProfissional()));
		entity.setFormacaoAcademica(dominioRepository.findByDominio(vo.getFormacao()));
		entity.setTipoRegistroProfissional(dominioRepository.findByDominio(vo.getTipoRegistro()));
		entity.setUfRegistro(ufRepository.findByCodUF(vo.getUfRegistro()));

		return entity;
	}

	public PessoaFisicaResponsavelTecnicoVO converterParaVO(PessoaFisicaResponsavelTecnicoEntity entity,
			PessoaFisicaResponsavelTecnicoVO vo) {

		vo.setDataRegistro(entity.getDataRegistro());
		vo.setnRegistroEstadual(entity.getNumeroRegistroProfissional());
		vo.setnRegistroNacional(entity.getNumeroRegProfissionalNacional());
		vo.setnVistoInscricaoSecudanria(entity.getVisto());

		vo.setCpf(entity.getPessoaFisica().getCpf());
		vo.setConselhoProfissional(entity.getConselhoProfissional().getDominio());
		vo.setFormacao(entity.getFormacaoAcademica().getDominio());
		vo.setTipoRegistro(entity.getTipoRegistroProfissional().getDominio());
		vo.setUfRegistro(entity.getUfRegistro().getCodigo());

		return vo;
	}

}
