package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.DadosJaCadastradosException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroInvalidoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.service.PessoaFisicaService;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaCompletaVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaVO;

@RestController
@RequestMapping("/pessoafisica")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PessoaFisicaController {
	
	private static final String HEADER_MESSAGE = "mensagem";


	@Autowired
	PessoaFisicaService pessoaFisicaService;
	
	@GetMapping
	public ResponseEntity<Page<PessoaFisicaVO>> buscaGeral(@PageableDefault(size = 20) Pageable pageable) {

		ResponseEntity<Page<PessoaFisicaVO>> response = null;

		try {
			response = ResponseEntity.ok(pessoaFisicaService.buscaTodos(pageable));
			
		} catch (Exception e) {
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	

	@GetMapping("/{cpf}")
	public ResponseEntity<PessoaFisicaVO> buscaPessoaFisicaPorCPF(@PathVariable String cpf) {

		ResponseEntity<PessoaFisicaVO> response = null;

		try {
			PessoaFisicaVO resultado = pessoaFisicaService.buscarPessoaFisicaPorCPF(cpf);
			response = ResponseEntity.ok(resultado);
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}

	@GetMapping({ "/visualiza/{cpf}", "/" })
	public ResponseEntity<PessoaFisicaCompletaVO> visualizaDadosPessoaFisica(
			@PathVariable(required = false) String cpf) {

		ResponseEntity<PessoaFisicaCompletaVO> response = null;

		try {
			PessoaFisicaCompletaVO resultado = pessoaFisicaService.buscaPessoaFisicaCompletaPorCPF(cpf);
			response = ResponseEntity.ok(resultado);
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}

	
	@PostMapping()
	public ResponseEntity<PessoaFisicaCompletaVO> salvarPessoaFisica(@RequestBody PessoaFisicaCompletaVO pessoaFisica) {

		ResponseEntity<PessoaFisicaCompletaVO> response = null;

		try {
			pessoaFisicaService.salvarPessoaFisica(pessoaFisica);
			ResponseEntity.ok().build();

		} catch (ParametroInvalidoException e) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (DadosJaCadastradosException e) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}

	@PutMapping()
	public ResponseEntity<PessoaFisicaCompletaVO> editarPessoaFisica(@RequestBody PessoaFisicaCompletaVO pessoaFisica) {

		ResponseEntity<PessoaFisicaCompletaVO> response = null;

		try {
			this.pessoaFisicaService.editarPessoaFisica(pessoaFisica);
			response = ResponseEntity.ok().build();

		} catch (ParametroInvalidoException e) {
			response = ResponseEntity.status(HttpStatus.BAD_REQUEST).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@PutMapping({"/ativar/{cpf}"})
	public ResponseEntity<Object> ativarPessoaFisica(
			@PathVariable(required = false) String cpf) {

		ResponseEntity<Object> response = null;

		try {
			pessoaFisicaService.ativaPessoaFisica(cpf);
			response = ResponseEntity.ok().build();
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@PutMapping({"/cancelar/{cpf}"})
	public ResponseEntity<Object> cancelarPessoaFisica(
			@PathVariable(required = false) String cpf) {

		ResponseEntity<Object> response = null;

		try {
			pessoaFisicaService.cancelarPessoaFisica(cpf);
			response = ResponseEntity.ok().build();
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@PutMapping({"/suspender/{cpf}"})
	public ResponseEntity<Object> suspenderPessoaFisica(
			@PathVariable(required = false) String cpf) {

		ResponseEntity<Object> response = null;

		try {
			pessoaFisicaService.suspendePessoaFisica(cpf);
			response = ResponseEntity.ok().build();
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@DeleteMapping("/{cpf}")
	public ResponseEntity<PessoaFisicaVO> deletaPessoaFisicaPorCPF(@PathVariable String cpf) {

		ResponseEntity<PessoaFisicaVO> response = null;

		try {
			pessoaFisicaService.deletaPessoaFisica(cpf);
			response = ResponseEntity.ok().build();
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}

}