package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.service.VinculoService;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoApiarioVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoAtividadeProdutivaVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoCNPJVO;

@RestController
@RequestMapping("/vinculo")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VinculoController {
	
	private static final String HEADER_MESSAGE = "mensagem";

	@Autowired
	VinculoService vinculoService;


	@GetMapping("/apiario/{cpf}")
	public ResponseEntity<Page<VinculoApiarioVO>> buscaVinculoApiario(@PathVariable(required = true) String cpf, @PageableDefault(size = 20) Pageable pageable) {

		ResponseEntity<Page<VinculoApiarioVO>> response = null;

		try {
			response = ResponseEntity.ok(vinculoService.vinculoApiario(cpf, pageable));
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@GetMapping("/cnpj/{cpf}")
	public ResponseEntity<Page<VinculoCNPJVO>> buscaVinculoCnpj(@PathVariable(required = true) String cpf, @PageableDefault(size = 20) Pageable pageable) {

		ResponseEntity<Page<VinculoCNPJVO>> response = null;

		try {
			response = ResponseEntity.ok(vinculoService.vinculoCNPJ(cpf, pageable));
		} catch (ParametroNaoEncontradoException e) {
			response = ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
	
	@GetMapping("/atividade/{cpf}")
	public ResponseEntity<Page<VinculoAtividadeProdutivaVO>> buscaVinculoAtividadeProdutiva(@PathVariable(required = true) String cpf, @PageableDefault(size = 20) Pageable pageable) {

		ResponseEntity<Page<VinculoAtividadeProdutivaVO>> response = null;

		try {
			response = ResponseEntity.ok(vinculoService.vinculoAP(cpf, pageable));
		} catch (ParametroNaoEncontradoException e) {
			ResponseEntity.status(HttpStatus.NOT_FOUND).header(HEADER_MESSAGE, e.getMessage()).build();
		} catch (Exception e) {
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).header(HEADER_MESSAGE, e.getMessage()).build();
		}

		return response;
	}
}
