package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaResponsavelTecnicoEntity;

public interface PessoaFisicaResponsavelTecnicoRepository
		extends PagingAndSortingRepository<PessoaFisicaResponsavelTecnicoEntity, Long> {

	@Query("SELECT rt FROM PessoaFisicaResponsavelTecnicoEntity rt WHERE rt.pessoaFisica.id = ?1")
	Page<PessoaFisicaResponsavelTecnicoEntity> findByIdPessoa(Long id, Pageable page);

	@Query("SELECT rt FROM PessoaFisicaResponsavelTecnicoEntity rt WHERE rt.pessoaFisica.cpf = ?1")
	Page<PessoaFisicaResponsavelTecnicoEntity> findByCpfPessoa(String cpf, Pageable page);

	@Query("SELECT rt FROM PessoaFisicaResponsavelTecnicoEntity rt WHERE rt.pessoaFisica.cpf = ?1 AND rt.formacaoAcademica.dominio = ?2")
	Optional<PessoaFisicaResponsavelTecnicoEntity> findByCpfPessoaAndFormacao(String cpf, String dominio);

	@Transactional
	@Modifying
	@Query("DELETE FROM PessoaFisicaResponsavelTecnicoEntity rt WHERE rt.pessoaFisica.cpf = ?1")
	void deleteCpfPessoa(String cpf);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM PessoaFisicaResponsavelTecnicoEntity rt WHERE rt.id = ?1")
	void deleteById(Long id);
}
