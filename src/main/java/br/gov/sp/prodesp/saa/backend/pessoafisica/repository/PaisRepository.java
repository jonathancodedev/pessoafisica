package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PaisEntity;

public interface PaisRepository extends  PagingAndSortingRepository<PaisEntity, Long>{
	
	PaisEntity findByNome(String pais);

}
