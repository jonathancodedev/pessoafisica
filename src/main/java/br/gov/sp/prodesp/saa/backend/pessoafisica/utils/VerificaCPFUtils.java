package br.gov.sp.prodesp.saa.backend.pessoafisica.utils;

import org.slf4j.Logger;

import br.com.caelum.stella.validation.CPFValidator;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroInvalidoException;

public class VerificaCPFUtils {

	public static void verificaCPF(String cpf, Logger logger) throws ParametroInvalidoException {
		logger.error("CPF INFORMADO É INVALIDO: {}", cpf);

		if (!new CPFValidator().isEligible(cpf.trim().replaceAll("[.-]", ""))) {
			throw new ParametroInvalidoException("CPF INFORMADO É INVALIDO");
		}
		
	};
}
