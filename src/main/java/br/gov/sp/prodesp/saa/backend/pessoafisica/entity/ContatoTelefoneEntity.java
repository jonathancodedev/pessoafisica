package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.gov.sp.prodesp.saa.backend.pessoafisica.interfaces.PopulaDadosDeSessao;

@Entity
@Table(name = "TB_CDA_CONTATO_TELEFONE")
public class ContatoTelefoneEntity extends UsuarioAtualizacao implements PopulaDadosDeSessao {
	
    public static final String DDI_DEFAULT = "55";
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDENT")
    private Long id;

    @Column(name = "NUM_TEL")
    private String telefone;

    @Column(name = "NUM_TEL_RAMAL")
    private String ramal;

    @Column(name = "COD_DDI")
    private String ddi = DDI_DEFAULT;

    @Column(name = "COD_DDD")
    private String ddd;

	public static String getDdiDefault() {
		return DDI_DEFAULT;
	}

	public String getTelefone() {
		return telefone;
	}

	public String getRamal() {
		return ramal;
	}

	public String getDdi() {
		return ddi;
	}

	public String getDdd() {
		return ddd;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public void setDdi(String ddi) {
		this.ddi = ddi;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
    
	@Override
	public void populaDadosDeSessao(UsuarioEntity usuarioAtualizacao, DominioEntity status, LocalDate dataAtualizacao) {
		this.popular(usuarioAtualizacao, status, dataAtualizacao);
	}
    

}
