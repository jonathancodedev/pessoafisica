package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaEntity;

@Repository
public interface PessoaRepository extends PagingAndSortingRepository<PessoaEntity, Long> {
	
	public Optional<PessoaEntity> findById(Long id);

}
