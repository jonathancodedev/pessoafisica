package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_APIARIO")
public class ApiarioEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	@Column(name = "CODIGO_APIARIO", nullable = true)
	private Long codigo;
	
	@OneToMany(mappedBy = "apiario", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
	private List<ApiarioResponsavelEntity> responsaveis;

	@Column(name = "NOME_APIARIO", nullable = false)
	private String nomeApiario;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_MUNICIPIO_IBGE", referencedColumnName = "IDENT", nullable = true)
	private MunicipioEntity municipio;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_DOM_STATUS", referencedColumnName = "IDENT", nullable = false)
	private DominioEntity status;

	public Long getId() {
		return id;
	}

	public Long getCodigo() {
		return codigo;
	}

	public List<ApiarioResponsavelEntity> getResponsaveis() {
		return responsaveis;
	}

	public String getNomeApiario() {
		return nomeApiario;
	}

	public MunicipioEntity getMunicipio() {
		return municipio;
	}

	public DominioEntity getStatus() {
		return status;
	}
	
	
}
