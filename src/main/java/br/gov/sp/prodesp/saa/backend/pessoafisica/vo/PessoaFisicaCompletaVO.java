package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

import java.time.LocalDate;

public class PessoaFisicaCompletaVO {
	
	String cpf;
	String rgRne;
	LocalDate dataEmissaoRG;
	LocalDate dataNascimento;
	String nome;
	String nomePai;
	String nomeMae;
	String orgaoEmissor;
	String observacoes;
	String ufOrgaoEmissorRg;
	String sexo;
	String ufNaturalidade;
	String estadoCivil;
	String nacionalidade;
	String contatoEmail;
	String orgaoEmissorRg;
	String municipioNaturalidade;
	
	ContatoTelefoneVO contatoTelefoneMovel;
	ContatoTelefoneVO contatoTelefoneComercial;
	ContatoTelefoneVO contatoTelefoneResidencial;
	EnderecoVO enderecoResidencial;
	EnderecoVO enderecoCorrespondencia;
	
	Boolean flagMesmoEndRes;
	Boolean flagRecebeEmail;
	Boolean flagRecebeSms;
	Boolean flagCaixaPostalCorr;
	Boolean flagRegistroTecnico;
	
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRgRne() {
		return rgRne;
	}
	public void setRgRne(String rgRne) {
		this.rgRne = rgRne;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomePai() {
		return nomePai;
	}
	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}
	public String getNomeMae() {
		return nomeMae;
	}
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}
	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}
	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public ContatoTelefoneVO getContatoTelefoneMovel() {
		return contatoTelefoneMovel;
	}
	public void setContatoTelefoneMovel(ContatoTelefoneVO contatoTelefoneMovel) {
		this.contatoTelefoneMovel = contatoTelefoneMovel;
	}
	public ContatoTelefoneVO getContatoTelefoneComercial() {
		return contatoTelefoneComercial;
	}
	public void setContatoTelefoneComercial(ContatoTelefoneVO contatoTelefoneComercial) {
		this.contatoTelefoneComercial = contatoTelefoneComercial;
	}
	public ContatoTelefoneVO getContatoTelefoneResidencial() {
		return contatoTelefoneResidencial;
	}
	public void setContatoTelefoneResidencial(ContatoTelefoneVO contatoTelefoneResidencial) {
		this.contatoTelefoneResidencial = contatoTelefoneResidencial;
	}
	public EnderecoVO getEnderecoResidencial() {
		return enderecoResidencial;
	}
	public void setEnderecoResidencial(EnderecoVO enderecoResidencial) {
		this.enderecoResidencial = enderecoResidencial;
	}
	public EnderecoVO getEnderecoCorrespondencia() {
		return enderecoCorrespondencia;
	}
	public void setEnderecoCorrespondencia(EnderecoVO enderecoCorrespondencia) {
		this.enderecoCorrespondencia = enderecoCorrespondencia;
	}
	public Boolean getFlagMesmoEndRes() {
		return flagMesmoEndRes;
	}
	public void setFlagMesmoEndRes(Boolean flagMesmoEndRes) {
		this.flagMesmoEndRes = flagMesmoEndRes;
	}
	public Boolean getFlagRecebeEmail() {
		return flagRecebeEmail;
	}
	public void setFlagRecebeEmail(Boolean flagRecebeEmail) {
		this.flagRecebeEmail = flagRecebeEmail;
	}
	public Boolean getFlagRecebeSms() {
		return flagRecebeSms;
	}
	public void setFlagRecebeSms(Boolean flagRecebeSms) {
		this.flagRecebeSms = flagRecebeSms;
	}
	public Boolean getFlagCaixaPostalCorr() {
		return flagCaixaPostalCorr;
	}
	public void setFlagCaixaPostalCorr(Boolean flagCaixaPostalCorr) {
		this.flagCaixaPostalCorr = flagCaixaPostalCorr;
	}
	public Boolean getFlagRegistroTecnico() {
		return flagRegistroTecnico;
	}
	public void setFlagRegistroTecnico(Boolean flagRegistroTecnico) {
		this.flagRegistroTecnico = flagRegistroTecnico;
	}
	public LocalDate getDataEmissaoRG() {
		return dataEmissaoRG;
	}
	public void setDataEmissaoRG(LocalDate dataEmissaoRG) {
		this.dataEmissaoRG = dataEmissaoRG;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getUfOrgaoEmissorRg() {
		return ufOrgaoEmissorRg;
	}
	public void setUfOrgaoEmissorRg(String ufOrgaoEmissorRg) {
		this.ufOrgaoEmissorRg = ufOrgaoEmissorRg;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getUfNaturalidade() {
		return ufNaturalidade;
	}
	public void setUfNaturalidade(String ufNaturalidade) {
		this.ufNaturalidade = ufNaturalidade;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public String getContatoEmail() {
		return contatoEmail;
	}
	public void setContatoEmail(String contatoEmail) {
		this.contatoEmail = contatoEmail;
	}
	public String getOrgaoEmissorRg() {
		return orgaoEmissorRg;
	}
	public void setOrgaoEmissorRg(String orgaoEmissorRg) {
		this.orgaoEmissorRg = orgaoEmissorRg;
	}
	public String getMunicipioNaturalidade() {
		return municipioNaturalidade;
	}
	public void setMunicipioNaturalidade(String municipioNaturalidade) {
		this.municipioNaturalidade = municipioNaturalidade;
	}
	
	
}
