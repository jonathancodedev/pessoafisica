package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class VinculoCNPJVO {
	
	String cnpj;
	String razaoSocial;
	String tipoPJ;
	String tipoVinculo;
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getTipoPJ() {
		return tipoPJ;
	}
	public void setTipoPJ(String tipoPJ) {
		this.tipoPJ = tipoPJ;
	}
	public String getTipoVinculo() {
		return tipoVinculo;
	}
	public void setTipoVinculo(String tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}
	
}
