package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ContatoTelefoneEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.CheckObjectUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.ContatoTelefoneVO;

public class ConverterTelefone {

	public ContatoTelefoneEntity converterParaEntidade(ContatoTelefoneEntity entity, ContatoTelefoneVO vo){
		entity.setDdd(vo.getDdd());
		entity.setDdi(vo.getDdi());
		entity.setTelefone(vo.getTelefone());
		entity.setRamal(CheckObjectUtils.checkString(vo.getRamal()));			
		
		return entity;
	}

	public ContatoTelefoneVO converterParaVO(ContatoTelefoneEntity entity, ContatoTelefoneVO vo) {
		vo.setDdd(entity.getDdd());
		vo.setDdi(entity.getDdi());
		vo.setTelefone(entity.getTelefone());
		vo.setRamal(CheckObjectUtils.checkString(entity.getRamal()));	
		
		return vo;
	}
}
