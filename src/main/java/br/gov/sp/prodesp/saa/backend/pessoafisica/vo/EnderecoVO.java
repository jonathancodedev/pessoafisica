package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class EnderecoVO {

	private String cep;
	private String numero;
	private String bairro;
	private String logradouro;
	private String complemento;
	private MunicipioVO municipio;
    private String caixaPostal;
    
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public MunicipioVO getMunicipio() {
		return municipio;
	}
	public void setMunicipio(MunicipioVO municipio) {
		this.municipio = municipio;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getCaixaPostal() {
		return caixaPostal;
	}
	public void setCaixaPostal(String caixaPostal) {
		this.caixaPostal = caixaPostal;
	}
    
    
}
