package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UnidadeAdministrativaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.UnidadeAdministrativaVO;

public class ConverterUnidadeAdministrativa {
	
	public static UnidadeAdministrativaEntity converterParaEntidade(UnidadeAdministrativaEntity entity, UnidadeAdministrativaVO vo){
		entity.setNome(vo.getNome());
		return entity;
	}

	public static UnidadeAdministrativaVO converterParaVO(UnidadeAdministrativaEntity entity, UnidadeAdministrativaVO vo) {
		vo.setNome(entity.getNome());
		return vo;
	} 

}
