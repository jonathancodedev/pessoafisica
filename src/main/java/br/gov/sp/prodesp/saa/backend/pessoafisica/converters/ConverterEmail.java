package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ContatoEletronicoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.ContatoEletronicoVO;

public class ConverterEmail {

	public static ContatoEletronicoEntity converterParaEntidade(ContatoEletronicoEntity entity, ContatoEletronicoVO vo){
		entity.setDescricao(vo.getDescricao());
		return entity;
	}

	public static ContatoEletronicoVO converterParaVO(ContatoEletronicoEntity entity, ContatoEletronicoVO vo) {
		vo.setDescricao(entity.getDescricao());
		return vo;
	}
	
}
