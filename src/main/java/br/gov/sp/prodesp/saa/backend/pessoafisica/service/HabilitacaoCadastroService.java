package br.gov.sp.prodesp.saa.backend.pessoafisica.service;

import java.time.LocalDate;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaHabilitacaoCredenciamentoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UsuarioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.DadosJaCadastradosException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroInvalidoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.DominioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaHabilitacaoCredenciamentoRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.UnidadeAdministrativaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.UsuarioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.CheckObjectUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.VerificaCPFUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.DeletarDadosHabilitacaoVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaHabilitacaoCredenciamentoVO;

@Service
public class HabilitacaoCadastroService {

	@Autowired
	PessoaFisicaHabilitacaoCredenciamentoRepository pessoaHabilitacaoCredenciamentoRepository;

	@Autowired
	UnidadeAdministrativaRepository unidadeAdministrativaRepository;

	@Autowired
	DominioRepository dominioRepository;

	@Autowired
	PessoaFisicaRepository pfRepository;

	@Autowired
	UsuarioRepository usuarioRepository;

	private static Logger logger = LoggerFactory.getLogger(HabilitacaoCadastroService.class);

	public Page<PessoaFisicaHabilitacaoCredenciamentoVO> buscaHabilitacaoCadastro(String cpf, Pageable page)
			throws Exception {

		Page<PessoaFisicaHabilitacaoCredenciamentoVO> retorno = null;

		try {
			logger.info("BUSCA NO BANCO POR HABILITAÇÕES OU CADASTROS DE PESSOA FISICA");
			Page<PessoaFisicaHabilitacaoCredenciamentoEntity> listaHabilitacao = pessoaHabilitacaoCredenciamentoRepository
					.findByCpf(cpf, page);

			if (listaHabilitacao.isEmpty()) {
				logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
				throw new ParametroNaoEncontradoException("CPF NÃO ENCONTRADO");
			}

			retorno = listaHabilitacao.map(habilitacao -> {
				PessoaFisicaHabilitacaoCredenciamentoVO habilitacaoVO = new PessoaFisicaHabilitacaoCredenciamentoVO();
				habilitacaoVO = converteParaVO(habilitacao, habilitacaoVO);
				return habilitacaoVO;
			});

		} catch (ParametroNaoEncontradoException e) {
			logger.error("Erro inesperado");
			throw new ParametroNaoEncontradoException("PARAMETRO NÃO ENCONTRADO NO BANCO", e);
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}

		return retorno;

	}

	public PessoaFisicaHabilitacaoCredenciamentoVO incluirHabilitacaoCadastro(PessoaFisicaHabilitacaoCredenciamentoVO habilitacaoVO) throws Exception { 
		
		VerificaCPFUtils.verificaCPF(habilitacaoVO.getCpf(), logger);
		
		logger.info("VERIFICA A EXISTENCIA DE HABILITAÇÕES OU CADASTROS DE PESSOA FISICA JÁ CADASTRADOS");
		Optional<PessoaFisicaHabilitacaoCredenciamentoEntity> check = pessoaHabilitacaoCredenciamentoRepository
				.findByCpfAndHabilitacao(habilitacaoVO.getCpf(), habilitacaoVO.getHabilitacaoCredenciamento());
		
		if (check.isPresent()) {
			logger.error("DADOS JÁ EXISTENTES NO BANCO");
			throw new DadosJaCadastradosException("Habilitação já existente no banco");
		}
		
		try {
			
			
			PessoaFisicaHabilitacaoCredenciamentoEntity habilitacaoEntity = converteParaEntidade(new PessoaFisicaHabilitacaoCredenciamentoEntity(), habilitacaoVO);
	
			UsuarioEntity usuario = usuarioRepository.findById(2L).get();
			DominioEntity status = dominioRepository.findById(101L).get();
			LocalDate now = LocalDate.now();
			habilitacaoEntity.populaDadosDeSessao(usuario, status, now);
			
			logger.info("SALVANDO HABILITAÇÃO OU CADASTRO DE PESSOA FISICA NO BANCO");
			pessoaHabilitacaoCredenciamentoRepository.save(habilitacaoEntity);
	
			return habilitacaoVO;
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	public PessoaFisicaHabilitacaoCredenciamentoVO alterarHabilitacaoCadastro(
			PessoaFisicaHabilitacaoCredenciamentoVO habilitacaoVO) throws Exception {
		try {
			VerificaCPFUtils.verificaCPF(habilitacaoVO.getCpf(), logger);
			logger.info("BUSCA NO BANCO POR HABILITAÇÃO OU CADASTRO DE PESSOA FISICA");
			PessoaFisicaHabilitacaoCredenciamentoEntity habilitacaoEntity = pessoaHabilitacaoCredenciamentoRepository
					.findByCpfAndHabilitacao(habilitacaoVO.getCpf(), habilitacaoVO.getHabilitacaoCredenciamento()).orElseThrow(() -> 
					new ParametroNaoEncontradoException("CPF NÃO ENCONTRADO"));
	
			habilitacaoEntity = converteParaEntidade(habilitacaoEntity, habilitacaoVO);
	
			logger.info("SALVANDO ALTERAÇÃO DE HABILITAÇÃO OU CADASTRO DE PESSOA FISICA NO BANCO");
			pessoaHabilitacaoCredenciamentoRepository.save(habilitacaoEntity);
	
			return habilitacaoVO;
			
		} catch (ParametroInvalidoException cne) {
			logger.error("CPF INFORMADO É INVALIDO: {}", habilitacaoVO.getCpf());
			throw new ParametroInvalidoException("CPF INFORMADO É INVALIDO: " + habilitacaoVO.getCpf(), cne);
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
			throw new ParametroNaoEncontradoException(e.getMessage());
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	public void deletarHabilitacaoCadastro(DeletarDadosHabilitacaoVO habilitacao) throws Exception {
		try {
			logger.info("DELETANDO DADOS DE HABILITAÇÃO OU CADASTRO DO BANCO");
			PessoaFisicaHabilitacaoCredenciamentoEntity habilitacaod = pessoaHabilitacaoCredenciamentoRepository
					.findByCpfAndHabilitacao(habilitacao.getCpf(), habilitacao.getHabilitacaoCredenciamento()).orElseThrow(() -> 
					new ParametroNaoEncontradoException("CPF NÃO ENCONTRADO"));
			pessoaHabilitacaoCredenciamentoRepository.deleteById(habilitacaod.getId());
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
			throw new ParametroNaoEncontradoException(e.getMessage());
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	public void deletarTodosHabilitacaoCadastro(String cpf, PageRequest page) throws Exception {
		try {
			logger.info("DELETANDO TODOS OS DADOS DE HABILITAÇÃO OU CADASTRO DO BANCO");
			if (!pessoaHabilitacaoCredenciamentoRepository.findByCpf(cpf, page).isEmpty()) {
				pessoaHabilitacaoCredenciamentoRepository.deleteAllByCpf(cpf);
			}
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	public PessoaFisicaHabilitacaoCredenciamentoEntity converteParaEntidade(
			PessoaFisicaHabilitacaoCredenciamentoEntity entity, PessoaFisicaHabilitacaoCredenciamentoVO vo) {

		entity.setCodDocumento(vo.getCodDocumento());
		entity.setDataFimValidade(vo.getDataFimValidade());

		entity.setNumeroProcessoSPI(CheckObjectUtils.checkString(vo.getNumeroProcessoSAA()));
		entity.setNumeroProcessoSemPapel(CheckObjectUtils.checkString(vo.getNumeroProcessoSPSemPapel()));
		entity.setObservacao(CheckObjectUtils.checkString(vo.getObservacao()));
		entity.setDataInicioValidade(CheckObjectUtils.checkLocalDate(vo.getDataEmissao()));

		entity.setPessoaFisica(pfRepository.findByCpf(vo.getCpf()).get());
		entity.setTipoHabilitacao(dominioRepository.findByDominioHabilitacao(vo.getTipoHabilitacao()));
		entity.setHabilitacaoCredenciamento(dominioRepository.findByDominio(vo.getHabilitacaoCredenciamento()));

		if (entity.getUnidadeAdministrativa() != null) {
			entity.setUnidadeAdministrativa(unidadeAdministrativaRepository.findByNome(vo.getEdaCadastro()));
		}
		return entity;
	}

	public PessoaFisicaHabilitacaoCredenciamentoVO converteParaVO(PessoaFisicaHabilitacaoCredenciamentoEntity entity,
			PessoaFisicaHabilitacaoCredenciamentoVO vo) {

		vo.setObservacao(entity.getObservacao());
		vo.setCodDocumento(entity.getCodDocumento());
		vo.setDataFimValidade(entity.getDataFimValidade());
		vo.setDataEmissao(entity.getDataInicioValidade());
		vo.setNumeroProcessoSAA(entity.getNumeroProcessoSPI());
		vo.setNumeroProcessoSPSemPapel(entity.getNumeroProcessoSemPapel());

		vo.setCpf(entity.getPessoaFisica().getCpf());
		vo.setTipoHabilitacao(entity.getTipoHabilitacao().getDominio());
		vo.setHabilitacaoCredenciamento(entity.getHabilitacaoCredenciamento().getDominio());

		if (entity.getUnidadeAdministrativa() != null) {
			vo.setEdaCadastro(entity.getUnidadeAdministrativa().getNome());
		}

		return vo;
	}

}
