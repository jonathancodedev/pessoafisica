package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import br.gov.sp.prodesp.saa.backend.pessoafisica.interfaces.PopulaDadosDeSessao;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "TB_CDA_PESSOA")
public class PessoaEntity extends UsuarioAtualizacao implements Serializable, PopulaDadosDeSessao{
	
	private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDENT")
    private Long id;

    @Column(name = "NOME")
    private String nome;
    
    @Column(name = "TIPO_PESSOA")
    protected char tipoPessoa;


	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public char getTipoPessoa() {
		return tipoPessoa;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTipoPessoa(char tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public void populaDadosDeSessao(UsuarioEntity usuarioAtualizacao, DominioEntity status, LocalDate dataAtualizacao) {
		this.popular(usuarioAtualizacao, status, dataAtualizacao);
	}
}
