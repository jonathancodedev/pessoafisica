package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioEntity;

public interface DominioRepository extends PagingAndSortingRepository<DominioEntity, Long> {
	
	@Query("SELECT de FROM DominioEntity de WHERE de.dominio = ?1")
	DominioEntity findByDominio(String estadoCivil);
	
	@Query("SELECT de FROM DominioEntity de WHERE de.dominio = ?1 and de.tipo = 43")
	DominioEntity findByDominioHabilitacao(String estadoCivil);
}
