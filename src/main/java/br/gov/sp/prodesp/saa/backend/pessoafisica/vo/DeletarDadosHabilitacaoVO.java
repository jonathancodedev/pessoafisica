package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class DeletarDadosHabilitacaoVO {
	
	String cpf;
	String habilitacaoCredenciamento;
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getHabilitacaoCredenciamento() {
		return habilitacaoCredenciamento;
	}
	public void setHabilitacaoCredenciamento(String habilitacaoCredenciamento) {
		this.habilitacaoCredenciamento = habilitacaoCredenciamento;
	}
	
	
}
