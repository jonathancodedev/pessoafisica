package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioTipoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.DominioTipoVO;

public class ConverterDominioTipo {

	public static DominioTipoEntity converterParaEntidade(DominioTipoEntity entity, DominioTipoVO vo){
		entity.setCodigo(vo.getCodigo());
		entity.setTipo(vo.getTipo());
		
		return entity;
	}

	public static DominioTipoVO converterParaVO(DominioTipoEntity entity, DominioTipoVO vo) {
		vo.setCodigo(entity.getCodigo());
		vo.setTipo(entity.getTipo());
		
		return vo;
	} 
}
