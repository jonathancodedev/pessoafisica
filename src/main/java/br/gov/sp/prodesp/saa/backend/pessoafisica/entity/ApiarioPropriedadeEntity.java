package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_APIARIO_PROPRIEDADE")
public class ApiarioPropriedadeEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_APIARIO", referencedColumnName = "IDENT", nullable = false)
	private ApiarioEntity apiario;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROPRIEDADE", referencedColumnName = "IDENT", nullable = false)
	private PropriedadeEntity propriedade;

	public Long getId() {
		return id;
	}

	public ApiarioEntity getApiario() {
		return apiario;
	}

	public PropriedadeEntity getPropriedade() {
		return propriedade;
	}
	
	


	
}
