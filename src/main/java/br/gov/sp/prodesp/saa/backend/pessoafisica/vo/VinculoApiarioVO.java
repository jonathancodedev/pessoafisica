package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class VinculoApiarioVO {
	
	Long codigoApiario; 
	String nomeApiario;
	Long codigoPropiedade;
	String nomePropiedade;
	String municipio;
	String tipoVinculo;
	String situacao;
	
	public String getNomeApiario() {
		return nomeApiario;
	}
	public void setNomeApiario(String nomeApiario) {
		this.nomeApiario = nomeApiario;
	}
	public String getNomePropiedade() {
		return nomePropiedade;
	}
	public void setNomePropiedade(String nomePropiedade) {
		this.nomePropiedade = nomePropiedade;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getTipoVinculo() {
		return tipoVinculo;
	}
	public void setTipoVinculo(String tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public Long getCodigoApiario() {
		return codigoApiario;
	}
	public void setCodigoApiario(Long codigoApiario) {
		this.codigoApiario = codigoApiario;
	}
	public Long getCodigoPropiedade() {
		return codigoPropiedade;
	}
	public void setCodigoPropiedade(Long codigoPropiedade) {
		this.codigoPropiedade = codigoPropiedade;
	}
	
	
}
