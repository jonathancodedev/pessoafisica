package br.gov.sp.prodesp.saa.backend.pessoafisica.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ApiarioPropriedadeEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ApiarioResponsavelEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.AtividadeProdutivaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PropiedadeAtividadeProdutivaProdutorEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.VinculoRamoAtividadePessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.ApiarioPessoaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.ApiarioPropriedadeRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PropiedadeAtividadeProdutivaProdutorRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.VinculoPJPFRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoApiarioVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoAtividadeProdutivaVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoCNPJVO;

@Service
public class VinculoService {
	
	@Autowired
	PessoaFisicaRepository pfRepository;
	
	@Autowired
	PropiedadeAtividadeProdutivaProdutorRepository atividadeProdutivaProdutorRepository;

	@Autowired
	VinculoPJPFRepository vinculoPJPFrepository;

	@Autowired
	ApiarioPessoaRepository apiarioPessoaRepository;

	@Autowired
	ApiarioPropriedadeRepository apiarioPropriedadeRepository;

	private static Logger logger = LoggerFactory.getLogger(VinculoService.class);

	public Page<VinculoAtividadeProdutivaVO> vinculoAP(String cpf, Pageable page) throws Exception {
		try {
			Page<PropiedadeAtividadeProdutivaProdutorEntity> vinculoAP;
	
			Page<VinculoAtividadeProdutivaVO> retorno = null;
	
			PessoaFisicaEntity pessoa = pfRepository.findByCpf(cpf).orElseThrow(() -> new ParametroNaoEncontradoException("PARAMETRO NÃO ENCONTRADO"));
			logger.info("BUSCA NO BANCO POR ATIVIDADES PRODUTIVAS RELACIONADAS A PESSOA FISICA");
			vinculoAP = this.atividadeProdutivaProdutorRepository.findAllByPessoa(pessoa.getId(), page);
			
			if (vinculoAP.isEmpty()) {
				throw new ParametroNaoEncontradoException("NÃO HÁ ATIVIDADES PRODUTIVAS RELACIONADAS");
			}
	
			retorno =  vinculoAP.map(atividadeProdutor -> {
				VinculoAtividadeProdutivaVO apVO = new VinculoAtividadeProdutivaVO();
				AtividadeProdutivaEntity atividadeProdutiva;
	
				apVO.setVinculo(atividadeProdutor.getTipoProdutor().getDominio());
				atividadeProdutiva = atividadeProdutor.getAtividadeProdutiva();
				apVO.setCodigo(atividadeProdutiva.getNumeroAtividadeProdutiva());
				apVO.setDescAtividadeProdutiva(atividadeProdutiva.getDescricao());
				apVO.setNomeMunicipio(
						atividadeProdutiva.getPropriedade().getEnderecoPropriedade().getMunicipio().getNome());
				apVO.setNomePropiedade(atividadeProdutiva.getPropriedade().getNome());
				apVO.setVertente(atividadeProdutiva.getVertente().getDominio());
				return apVO;
			});
	
			return retorno;
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage());
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

	public Page<VinculoCNPJVO> vinculoCNPJ(String cpf, Pageable page) throws Exception {
		
		try {
			Page<VinculoRamoAtividadePessoaFisicaEntity> vinculoPJPF;
	
			Page<VinculoCNPJVO> retorno = null;
			
			logger.info("BUSCA PESSOA FISICA POR CPF NO BANCO");
			PessoaFisicaEntity pessoa = pfRepository.findByCpf(cpf).orElseThrow(() -> new ParametroNaoEncontradoException("PARAMETRO NÃO ENCONTRADO"));
	
			logger.info("BUSCA NO BANCO POR PESSOA JURIDICA RAMO DE ATIVIDADE RELACIONADO A PESSOA FISICA");
			vinculoPJPF = vinculoPJPFrepository.findAllByPessoa(pessoa.getId(), page);
			
			if (vinculoPJPF.isEmpty()) {
				logger.error("PARAMETRO NÃO ENCONTRADO NO BANCO");
				throw new ParametroNaoEncontradoException("NÃO HÁ ATIVIDADES PRODUTIVAS RELACIONADAS");
			}
			
			retorno = vinculoPJPF.map(vinculo -> {
				VinculoCNPJVO vinculoVO = new VinculoCNPJVO();
				vinculoVO.setCnpj(vinculo.getRamoAtividade().getPessoaJuridica().getCnpj());
				vinculoVO.setRazaoSocial(vinculo.getRamoAtividade().getPessoaJuridica().getNome());
				vinculoVO.setTipoPJ(vinculo.getRamoAtividade().getRamoAtividade().getDominio());
				vinculoVO.setTipoVinculo(vinculo.getTipoVinculo().getDominio());
				return vinculoVO;
			});
	
			return retorno;
		
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage());
				
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}

	}

	public Page<VinculoApiarioVO> vinculoApiario(String cpf, Pageable page) throws Exception {
		try {
			Page<ApiarioResponsavelEntity> listaResponsavel;
			
			Page<VinculoApiarioVO> retorno = null;
	
			logger.info("BUSCA PESSOA FISICA POR CPF NO BANCO");
			PessoaFisicaEntity pessoa = pfRepository.findByCpf(cpf).orElseThrow(() -> new ParametroNaoEncontradoException("PARAMETRO NÃO ENCONTRADO"));
	
			logger.info("BUSCA NO BANCO VINCULOS DE PESSOA COM APIARIO);");
			listaResponsavel = apiarioPessoaRepository.findByPessoa(pessoa.getId(), page);
			
			if (listaResponsavel.isEmpty()) {
				throw new ParametroNaoEncontradoException("NÃO HÁ ATIVIDADES PRODUTIVAS RELACIONADAS");
			}
			
			retorno = listaResponsavel.map(responsavel -> {
				ApiarioPropriedadeEntity apiario;
				VinculoApiarioVO vinculoApiario = new VinculoApiarioVO();
				vinculoApiario.setCodigoApiario(responsavel.getApiario().getCodigo());
				vinculoApiario.setNomeApiario(responsavel.getApiario().getNomeApiario());
				vinculoApiario.setSituacao(responsavel.getApiario().getStatus().getDominio());
				vinculoApiario.setMunicipio(responsavel.getApiario().getMunicipio().getNome());
				vinculoApiario.setTipoVinculo(responsavel.getRamoAtividade() != null
						? responsavel.getRamoAtividade().getRamoAtividade().getDominio()
						: null);
	
				apiario = apiarioPropriedadeRepository.findByApiarioId(responsavel.getApiario().getId());
				if (apiario != null) {
					vinculoApiario.setCodigoPropiedade(
							apiario.getPropriedade() != null ? apiario.getPropriedade().getNumeroPropriedade() : null);
					vinculoApiario.setNomePropiedade(
							apiario.getPropriedade() != null ? apiario.getPropriedade().getNome() : null);
				}
				
				return vinculoApiario;
			});
	
			return retorno;
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage());
				
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}

}
