package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.gov.sp.prodesp.saa.backend.pessoafisica.interfaces.PopulaDadosDeSessao;

@Entity
@Table(name = "TB_CDA_ENDERECO")
public class EnderecoEntity extends UsuarioAtualizacao implements  PopulaDadosDeSessao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	@Column(name = "NUM_LOGR", nullable = true)
	private String numero;

	@JoinColumn(name = "ID_MUNICIPIO_IBGE", referencedColumnName = "IDENT", nullable = true)
	@ManyToOne
	private MunicipioEntity municipio;

	@Column(name = "COMPLEM_END", nullable = true)
	private String complemento;

	@Column(name = "BAIRRO", nullable = true)
	private String bairro;

	@Column(name = "COD_CEP", nullable = true)
	private String cep;

	@Column(name = "DESCR_LOGR", nullable = true)
	private String logradouro;
	
    @Column(name = "CAIXA_POSTAL", nullable = true)
    private String caixaPostal;
    
	public Long getId() {
		return id;
	}

	public String getNumero() {
		return numero;
	}

	public MunicipioEntity getMunicipio() {
		return municipio;
	}

	public String getComplemento() {
		return complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCep() {
		return cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getCaixaPostal() {
		return caixaPostal;
	}

	public void setMunicipio(MunicipioEntity municipio) {
		this.municipio = municipio;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setCaixaPostal(String caixaPostal) {
		this.caixaPostal = caixaPostal;
	}
	
	@Override
	public void populaDadosDeSessao(UsuarioEntity usuarioAtualizacao, DominioEntity status, LocalDate dataAtualizacao) {
		this.popular(usuarioAtualizacao, status, dataAtualizacao);
	}

}
