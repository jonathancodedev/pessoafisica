package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

public class ContatoEletronicoVO {
	
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
