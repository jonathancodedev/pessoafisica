package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.VinculoRamoAtividadePessoaFisicaEntity;

@Repository
public interface VinculoPJPFRepository extends PagingAndSortingRepository<VinculoRamoAtividadePessoaFisicaEntity, Long>{
	
	@Query("SELECT vpf FROM VinculoRamoAtividadePessoaFisicaEntity vpf WHERE vpf.pessoaFisica.id = ?1")
	Page<VinculoRamoAtividadePessoaFisicaEntity> findAllByPessoa(Long id, Pageable page);

}
