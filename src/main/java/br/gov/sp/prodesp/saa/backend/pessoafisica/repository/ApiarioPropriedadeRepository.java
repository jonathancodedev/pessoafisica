package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ApiarioPropriedadeEntity;

@Repository
public interface ApiarioPropriedadeRepository extends PagingAndSortingRepository<ApiarioPropriedadeEntity, Long> {

	@Query("SELECT ap FROM ApiarioPropriedadeEntity ap " + "WHERE ap.apiario.id = ?1")
	ApiarioPropriedadeEntity findByApiarioId(Long codigo);

	
}
