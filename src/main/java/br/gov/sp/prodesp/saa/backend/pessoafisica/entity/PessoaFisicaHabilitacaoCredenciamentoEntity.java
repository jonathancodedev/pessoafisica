package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.gov.sp.prodesp.saa.backend.pessoafisica.interfaces.PopulaDadosDeSessao;

@Entity
@Table(name = "TB_CDA_PESSOA_FISICA_HABILITACAO_CREDENCIAMENTO")
public class PessoaFisicaHabilitacaoCredenciamentoEntity extends UsuarioAtualizacao implements  PopulaDadosDeSessao{
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDENT")
    private Long id;

	@Column(name = "COD_DOCTO")
	private String codDocumento;

	@Column(name = "DATA_INICIO_VALID")
	private LocalDate dataInicioValidade;

	@Column(name = "DATA_FIM_VALID")
	private LocalDate dataFimValidade;

	@Column(name = "DATA_SUSPENSAO")
	private LocalDate dataSuspensao;
	
    @Column(name = "DESCR_OBSERV")
    private String observacao;

    @Column(name = "NUM_PROCES_SPI")
    private String numeroProcessoSPI;
    
    @Column(name = "NUM_PROCESSO_SEM_PAPEL")
    private String numeroProcessoSemPapel;
    
	@JoinColumn(name = "ID_PESSOA_FISICA", referencedColumnName = "ID_PESSOA")
	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	private PessoaFisicaEntity pessoaFisica;

	@JoinColumn(name = "ID_DOM_TIPO_HABILIT_CREDENC", referencedColumnName = "IDENT")
	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	private DominioEntity tipoHabilitacao;

	@JoinColumn(name = "ID_HABILITACAO_CREDENCIAMENTO", referencedColumnName = "IDENT")
	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	private DominioEntity habilitacaoCredenciamento;
	
	@JoinColumn(name = "ID_DOM_TIPO_MOTIVO_SUSPENSAO", referencedColumnName = "IDENT")
	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	private DominioEntity tipoMotivoSuspensao;

	@JoinColumn(name = "ID_UNIDADE_ADMINISTRATIVA", referencedColumnName = "IDENT")
	@ManyToOne(optional = true, cascade = CascadeType.ALL)
	private UnidadeAdministrativaEntity unidadeAdministrativa;

	public String getCodDocumento() {
		return codDocumento;
	}

	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}

	public LocalDate getDataInicioValidade() {
		return dataInicioValidade;
	}

	public void setDataInicioValidade(LocalDate dataInicioValidade) {
		this.dataInicioValidade = dataInicioValidade;
	}

	public LocalDate getDataFimValidade() {
		return dataFimValidade;
	}

	public void setDataFimValidade(LocalDate dataFimValidade) {
		this.dataFimValidade = dataFimValidade;
	}

	public LocalDate getDataSuspensao() {
		return dataSuspensao;
	}

	public void setDataSuspensao(LocalDate dataSuspensao) {
		this.dataSuspensao = dataSuspensao;
	}

	public PessoaFisicaEntity getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisicaEntity pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public DominioEntity getTipoHabilitacao() {
		return tipoHabilitacao;
	}

	public void setTipoHabilitacao(DominioEntity tipoHabilitacao) {
		this.tipoHabilitacao = tipoHabilitacao;
	}

	public DominioEntity getHabilitacaoCredenciamento() {
		return habilitacaoCredenciamento;
	}

	public void setHabilitacaoCredenciamento(DominioEntity habilitacaoCredenciamento) {
		this.habilitacaoCredenciamento = habilitacaoCredenciamento;
	}

	public DominioEntity getTipoMotivoSuspensao() {
		return tipoMotivoSuspensao;
	}

	public void setTipoMotivoSuspensao(DominioEntity tipoMotivoSuspensao) {
		this.tipoMotivoSuspensao = tipoMotivoSuspensao;
	}

	public UnidadeAdministrativaEntity getUnidadeAdministrativa() {
		return unidadeAdministrativa;
	}

	public void setUnidadeAdministrativa(UnidadeAdministrativaEntity unidadeAdministrativa) {
		this.unidadeAdministrativa = unidadeAdministrativa;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroProcessoSPI() {
		return numeroProcessoSPI;
	}

	public void setNumeroProcessoSPI(String numeroProcessoSPI) {
		this.numeroProcessoSPI = numeroProcessoSPI;
	}

	public String getNumeroProcessoSemPapel() {
		return numeroProcessoSemPapel;
	}

	public void setNumeroProcessoSemPapel(String numeroProcessoSemPapel) {
		this.numeroProcessoSemPapel = numeroProcessoSemPapel;
	}
	
	@Override
	public void populaDadosDeSessao(UsuarioEntity usuarioAtualizacao, DominioEntity status, LocalDate dataAtualizacao) {
		this.popular(usuarioAtualizacao, status, dataAtualizacao);
	}
}
