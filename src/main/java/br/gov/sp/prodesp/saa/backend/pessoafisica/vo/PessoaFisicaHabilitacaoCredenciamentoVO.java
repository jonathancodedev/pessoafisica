package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

import java.time.LocalDate;

public class PessoaFisicaHabilitacaoCredenciamentoVO {

	private String cpf;
	private String codDocumento;
	private String numeroProcessoSAA;
	private String numeroProcessoSPSemPapel;
	private String observacao;
	private LocalDate dataEmissao;
	private LocalDate dataFimValidade;
	private LocalDate dataSuspensao;

	private String tipoHabilitacao;
	private String habilitacaoCredenciamento;
	private String edaCadastro;
	private String tipoMotivoSuspensao;
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCodDocumento() {
		return codDocumento;
	}
	public void setCodDocumento(String codDocumento) {
		this.codDocumento = codDocumento;
	}
	public LocalDate getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(LocalDate dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public LocalDate getDataFimValidade() {
		return dataFimValidade;
	}
	public void setDataFimValidade(LocalDate dataFimValidade) {
		this.dataFimValidade = dataFimValidade;
	}
	public LocalDate getDataSuspensao() {
		return dataSuspensao;
	}
	public void setDataSuspensao(LocalDate dataSuspensao) {
		this.dataSuspensao = dataSuspensao;
	}
	public String getNumeroProcessoSAA() {
		return numeroProcessoSAA;
	}
	public void setNumeroProcessoSAA(String numeroProcessoSAA) {
		this.numeroProcessoSAA = numeroProcessoSAA;
	}
	public String getNumeroProcessoSPSemPapel() {
		return numeroProcessoSPSemPapel;
	}
	public void setNumeroProcessoSPSemPapel(String numeroProcessoSPSemPapel) {
		this.numeroProcessoSPSemPapel = numeroProcessoSPSemPapel;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getTipoHabilitacao() {
		return tipoHabilitacao;
	}
	public void setTipoHabilitacao(String tipoHabilitacao) {
		this.tipoHabilitacao = tipoHabilitacao;
	}
	public String getHabilitacaoCredenciamento() {
		return habilitacaoCredenciamento;
	}
	public void setHabilitacaoCredenciamento(String habilitacaoCredenciamento) {
		this.habilitacaoCredenciamento = habilitacaoCredenciamento;
	}
	public String getEdaCadastro() {
		return edaCadastro;
	}
	public void setEdaCadastro(String edaCadastro) {
		this.edaCadastro = edaCadastro;
	}
	public String getTipoMotivoSuspensao() {
		return tipoMotivoSuspensao;
	}
	public void setTipoMotivoSuspensao(String tipoMotivoSuspensao) {
		this.tipoMotivoSuspensao = tipoMotivoSuspensao;
	}
	
    
}