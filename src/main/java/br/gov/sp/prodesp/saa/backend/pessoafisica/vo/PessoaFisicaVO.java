package br.gov.sp.prodesp.saa.backend.pessoafisica.vo;

import java.time.LocalDate;

public class PessoaFisicaVO {
	
	String cpf;
	String rg;
	String nome;
	String municipio;
	String situacao;
	LocalDate dataAlteracao;
	String cpfUsuarioAtualizacao;
	String usuarioAtualizacaoNome;
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getRg() {
		return rg;
	}
	
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getMunicipio() {
		return municipio;
	}
	
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	
	public String getSituacao() {
		return situacao;
	}
	
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	public LocalDate getDataAlteracao() {
		return dataAlteracao;
	}
	
	public void setDataAlteracao(LocalDate localDate) {
		this.dataAlteracao = localDate;
	}
	
	public String getCpfUsuarioAtualizacao() {
		return cpfUsuarioAtualizacao;
	}
	
	public void setCpfUsuarioAtualizacao(String cpfUsuarioAtualizacao) {
		this.cpfUsuarioAtualizacao = cpfUsuarioAtualizacao;
	}
	
	public String getUsuarioAtualizacaoNome() {
		return usuarioAtualizacaoNome;
	}
	
	public void setUsuarioAtualizacaoNome(String usuarioAtualizacaoNome) {
		this.usuarioAtualizacaoNome = usuarioAtualizacaoNome;
	}
	
	
}
