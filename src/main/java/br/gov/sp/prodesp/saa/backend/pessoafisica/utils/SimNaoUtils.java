package br.gov.sp.prodesp.saa.backend.pessoafisica.utils;

public class SimNaoUtils {
	
	private SimNaoUtils () {}
	
	public static Boolean converteParaBoolean(Character x) {
		Boolean simNao = false;
		
		if (x.equals('S')) {
			simNao = true;
		}
		
		return simNao;
	}
	
	public static char converteParaChar(Boolean x) {
		char simNao = 'N';
		
		if (x) {
			simNao = 'S';
		}
		
		return simNao;
	}


}
