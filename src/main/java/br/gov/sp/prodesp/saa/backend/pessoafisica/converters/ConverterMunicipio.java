package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.MunicipioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.MunicipioVO;

public class ConverterMunicipio {
	
	public static MunicipioVO converterParaVO(MunicipioEntity  entity, MunicipioVO vo) {
		vo.setNome(entity.getNome());
		vo.setUf(entity.getUf().getCodigo());
		
		return vo;
	} 
	
}
