package br.gov.sp.prodesp.saa.backend.pessoafisica.converters;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioTipoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.DominioTipoVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.DominioVO;

public class ConverterDominio {
	
	public static DominioEntity converterParaEntidade(DominioEntity entity, DominioVO vo){
		entity.setCodigo(vo.getCodigo());
		entity.setDominio(vo.getDominio());
		entity.setTipo(ConverterDominioTipo.converterParaEntidade(new DominioTipoEntity(), vo.getTipo()));
		
		return entity;
	}

	public static DominioVO converterParaVO(DominioEntity entity, DominioVO vo) {
		vo.setCodigo(entity.getCodigo());
		vo.setDominio(entity.getDominio());
		vo.setTipo(ConverterDominioTipo.converterParaVO(entity.getTipo(), new DominioTipoVO()));
		
		return vo;
	} 
	

}
