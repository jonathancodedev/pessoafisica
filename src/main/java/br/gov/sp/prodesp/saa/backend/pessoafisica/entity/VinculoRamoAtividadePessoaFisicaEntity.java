package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_VINCULO_PJ_PF_RAMO")
public class VinculoRamoAtividadePessoaFisicaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;
	
    @ManyToOne
    @JoinColumn(name = "ID_PF", referencedColumnName = "ID_PESSOA")
    private PessoaFisicaEntity pessoaFisica;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_VINCULO", referencedColumnName = "IDENT")
    private DominioEntity tipoVinculo;
    
    @ManyToOne
    @JoinColumn(name = "ID_DOM_STATUS", referencedColumnName = "IDENT")
    private DominioEntity status;
	
    @ManyToOne
    @JoinColumn(name = "ID_PESSOA_JURIDICA_RAMO_ATIVIDADE", referencedColumnName = "IDENT")
    private PessoaJuridicaRamoAtividadeEntity ramoAtividade;


	public Long getId() {
		return id;
	}


	public PessoaFisicaEntity getPessoaFisica() {
		return pessoaFisica;
	}


	public DominioEntity getTipoVinculo() {
		return tipoVinculo;
	}


	public PessoaJuridicaRamoAtividadeEntity getRamoAtividade() {
		return ramoAtividade;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setPessoaFisica(PessoaFisicaEntity pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}


	public void setTipoVinculo(DominioEntity tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}


	public void setRamoAtividade(PessoaJuridicaRamoAtividadeEntity ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}


	public DominioEntity getStatus() {
		return status;
	}


	public void setStatus(DominioEntity status) {
		this.status = status;
	}
	
    
}
