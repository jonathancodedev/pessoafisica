package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_PROPRIEDADE")
public class PropriedadeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;

	@Column(name = "NOME_PROPRI", nullable = false)
	private String nome;

	@JoinColumn(name = "ID_ENDERECO_PROPRIEDADE", nullable = false)
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	private EnderecoEntity enderecoPropriedade;

	@Column(name = "NUM_PROPRI", nullable = true)
	private Long numeroPropriedade;

	public String getNome() {
		return nome;
	}

	public EnderecoEntity getEnderecoPropriedade() {
		return enderecoPropriedade;
	}

	public Long getId() {
		return id;
	}

	public Long getNumeroPropriedade() {
		return numeroPropriedade;
	}

	
}
