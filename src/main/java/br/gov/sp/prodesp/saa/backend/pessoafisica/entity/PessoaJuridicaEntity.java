package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_PESSOA_JURIDICA")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class PessoaJuridicaEntity extends PessoaEntity {

	private static final long serialVersionUID = 1L;
	@Column(name = "COD_CNPJ")
	private String cnpj;
	
	public String getCnpj() {
		return cnpj;
	}

	
}
