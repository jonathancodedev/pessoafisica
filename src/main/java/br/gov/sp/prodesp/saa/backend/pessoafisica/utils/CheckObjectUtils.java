package br.gov.sp.prodesp.saa.backend.pessoafisica.utils;

import java.time.LocalDate;

public class CheckObjectUtils {

	private CheckObjectUtils() {}

	public static String checkString(String object) {
		return object != null || object != "" ? object : null;
	}
	
	public static LocalDate checkLocalDate(LocalDate object) {
		return object != null ? object : null;
	}
	
}
