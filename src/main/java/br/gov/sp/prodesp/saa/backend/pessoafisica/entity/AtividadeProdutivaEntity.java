package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CDA_PROPRIEDADE_ATIVIDADE_PRODUTIVA_AP")
public class AtividadeProdutivaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;
	
	@Column(name = "DESCR_ATIVID_PRODUT", length = 128, nullable = false)
	private String descricao;

    @ManyToOne//(fetch = FetchType.LAZY) 
    @JoinColumn(name = "ID_PROPRIEDADE", referencedColumnName = "IDENT", nullable = false)
    private PropriedadeEntity propriedade;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ID_DOM_VERTENTE", referencedColumnName = "IDENT")
    private DominioEntity vertente;
    
    @Column(name = "NUM_ATIVID_PRODUT", nullable = true)
    private Long numeroAtividadeProdutiva;


	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public PropriedadeEntity getPropriedade() {
		return propriedade;
	}

	public DominioEntity getVertente() {
		return vertente;
	}

	public Long getNumeroAtividadeProdutiva() {
		return numeroAtividadeProdutiva;
	}
	
	
    
    

}
