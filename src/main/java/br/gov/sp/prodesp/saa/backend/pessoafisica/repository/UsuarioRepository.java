package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UsuarioEntity;

public interface UsuarioRepository  extends PagingAndSortingRepository<UsuarioEntity, Long> {

}
