package br.gov.sp.prodesp.saa.backend.pessoafisica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class PessoaFisicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PessoaFisicaApplication.class, args);
	}

}
