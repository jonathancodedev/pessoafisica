package br.gov.sp.prodesp.saa.backend.pessoafisica.interfaces;

import java.time.LocalDate;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UsuarioEntity;

public interface PopulaDadosDeSessao {
	void populaDadosDeSessao(UsuarioEntity usuarioAtualizacao, DominioEntity status, LocalDate dataAtualizacao);
}
