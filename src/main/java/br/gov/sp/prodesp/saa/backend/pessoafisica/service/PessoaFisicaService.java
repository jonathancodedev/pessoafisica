package br.gov.sp.prodesp.saa.backend.pessoafisica.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.gov.sp.prodesp.saa.backend.pessoafisica.converters.ConverterEndereco;
import br.gov.sp.prodesp.saa.backend.pessoafisica.converters.ConverterTelefone;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ContatoEletronicoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ContatoTelefoneEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.DominioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.EnderecoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.OcorrenciaPessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaHabilitacaoCredenciamentoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaResponsavelTecnicoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PropiedadeAtividadeProdutivaProdutorEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.UsuarioEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.VinculoRamoAtividadePessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.DadosJaCadastradosException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ParametroNaoEncontradoException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.exception.ServiceException;
import br.gov.sp.prodesp.saa.backend.pessoafisica.interfaces.PopulaDadosDeSessao;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.DominioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.MunicipioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.OcorrenciaPessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PaisRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaHabilitacaoCredenciamentoRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaResponsavelTecnicoRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PropiedadeAtividadeProdutivaProdutorRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.UFRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.UsuarioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.VinculoPJPFRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.CheckObjectUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.SimNaoUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.utils.VerificaCPFUtils;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.ContatoTelefoneVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.EnderecoVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaCompletaVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaVO;

@Service
public class PessoaFisicaService {

	@Autowired
	PessoaFisicaRepository pfRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	OcorrenciaPessoaFisicaRepository ocorrenciaRepository;

	@Autowired
	DominioRepository dominioRepository;

	@Autowired
	PessoaFisicaResponsavelTecnicoRepository pessoaResponsavelRepository;

	@Autowired
	PessoaFisicaHabilitacaoCredenciamentoRepository pessoaHabilitacaoCredenciamentoRepository;
	
	@Autowired
	UFRepository ufRepository;
	
	@Autowired
	MunicipioRepository municipioRepository;
	
	@Autowired
	PaisRepository paisRepository;
	
	@Autowired
	PropiedadeAtividadeProdutivaProdutorRepository atividadeProdutivaProdutorRepository;

	@Autowired
	VinculoPJPFRepository vinculoPJPFrepository;
	
	@Autowired
	RegistroProfissionalService registroProfissionalService;
	
	@Autowired
	HabilitacaoCadastroService habilitacaoCadastroService;

	ConverterTelefone conversorTelefone = new ConverterTelefone();
	ConverterEndereco conversorEndereco = new ConverterEndereco();

	private static Logger logger = LoggerFactory.getLogger(PessoaFisicaService.class);
	
	public Page<PessoaFisicaVO> buscaTodos(Pageable pageable) throws Exception {
		
		Page<PessoaFisicaVO> retorno = null;
		
		try {
			logger.info("INICIANDO PESQUISA CONSULTA GERAL DE PESSOA FISICA");
			Page<PessoaFisicaEntity> listaPessoa = pfRepository.findAll(pageable);
			
			retorno =  listaPessoa.map(pessoaFisica -> {
				PessoaFisicaVO vo = new PessoaFisicaVO();
				vo = converterParaVO(pessoaFisica, vo);

				return vo;
			});
			
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			
			throw new Exception("Erro inesperado", e);
		}
		
		return retorno;
	}

	public PessoaFisicaVO buscarPessoaFisicaPorCPF(String cpf) throws ServiceException {

		try {
			logger.info("INICIANDO PESQUISA CONSULTA DE PESSOA FISICA: {}", cpf);
			PessoaFisicaEntity pessoaFisica = null;
			PessoaFisicaVO pessoaVO = new PessoaFisicaVO();

			logger.info("BUSCA PESSOA FISICA NO BANCO");
			pessoaFisica = pfRepository.findByCpf(cpf)
					.orElseThrow(() -> new ParametroNaoEncontradoException("CPF NAO ENCONTRADO!"));

			pessoaVO = converterParaVO(pessoaFisica, pessoaVO);
			return pessoaVO;
			
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage(), e);

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("FALHA AO CONSULTAR CPF");
			throw new ServiceException("FALHA AO CONSULTAR CPF", ex);
		}

	}

	public PessoaFisicaCompletaVO buscaPessoaFisicaCompletaPorCPF(String cpf) throws ServiceException {
		logger.info("BUSCA DADOS COMPLETOS DE PESSOA FISICA");
		PessoaFisicaEntity pessoaFisica = null;
		PessoaFisicaCompletaVO vpVO = new PessoaFisicaCompletaVO();

		try {
			logger.info("PESQUISA PESSOA FISICA NO BANCO");
			pessoaFisica = pfRepository.findByCpf(cpf)
					.orElseThrow(() -> new ParametroNaoEncontradoException("NENHUM DADO ENCONTRADO COM O CPF: " + cpf));
			
			vpVO = converterParaVOCompleto(pessoaFisica, vpVO);
		
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage(), e);

		} catch (Exception ex) {
			logger.error("FALHA AO CONSULTAR CPF");
			throw new ServiceException("FALHA AO CONSULTAR CPF", ex);
		}

		return vpVO;

	}

	@Transactional
	public void salvarPessoaFisica(PessoaFisicaCompletaVO pessoaFisica) throws Exception {
		VerificaCPFUtils.verificaCPF(pessoaFisica.getCpf(), logger);
			
		logger.info("VERIFIFICANDO A EXISTENCIA DE PESSOA FISICA JÁ CADASTRADA NO BANCO");
		if(pfRepository.findByCpf(pessoaFisica.getCpf()).isPresent()) {
			logger.error("DADOS JÁ EXISTENTES NO BANCO");
			throw new DadosJaCadastradosException("Pessoa Fisica já cadastrada com esse CPF");
		}
		
		try {

			
			List<PopulaDadosDeSessao> sessoes = new ArrayList<>();
		
			PessoaFisicaEntity pessoaFisicaEntity = new PessoaFisicaEntity();
			pessoaFisicaEntity.setContatoEmail(new ContatoEletronicoEntity());
			
			pessoaFisicaEntity = converterParaEntity(pessoaFisicaEntity, pessoaFisica);
			
			sessoes.add(pessoaFisicaEntity);
			sessoes.add(pessoaFisicaEntity.getContatoEmail());
			sessoes.add(pessoaFisicaEntity.getEnderecoResidencial());
			sessoes.add(pessoaFisicaEntity.getEnderecoCorrespondencia());
			
			if (pessoaFisica.getContatoTelefoneComercial() != null) {
				sessoes.add(pessoaFisicaEntity.getContatoTelefoneComercial());
			}
			
			if (pessoaFisica.getContatoTelefoneMovel() != null) {
				sessoes.add(pessoaFisicaEntity.getContatoTelefoneMovel());
			}
			
			if (pessoaFisica.getContatoTelefoneResidencial() != null) {
				sessoes.add(pessoaFisicaEntity.getContatoTelefoneResidencial());
			}
			
			UsuarioEntity usuario = usuarioRepository.findById(2L).get();
			DominioEntity status = dominioRepository.findById(101L).get();
			LocalDate now = LocalDate.now();
			
			for (PopulaDadosDeSessao sessao : sessoes) {
				sessao.populaDadosDeSessao(usuario, status, now);
			}
			
			logger.info("SALVANDO PESSOA FISICA NO BANCO");
			this.pfRepository.save(pessoaFisicaEntity);
			
		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
		
	};

	public void editarPessoaFisica(PessoaFisicaCompletaVO pessoaFisica ) throws Exception {
		
		VerificaCPFUtils.verificaCPF(pessoaFisica.getCpf(), logger);
		
		try {			

			logger.info("BUSCANDO PESSOA FISICA NO BANCO PARA ATUALIZAR DADOS");
			PessoaFisicaEntity pessoaFisicaEntity = pfRepository.findByCpf(pessoaFisica.getCpf()).orElseThrow(() -> new ParametroNaoEncontradoException("CPF NAO ENCONTRADO!"));
	
			converterParaEntity(pessoaFisicaEntity, pessoaFisica);
			
			List<PopulaDadosDeSessao> sessoes = new ArrayList<>();
			
			sessoes.add(pessoaFisicaEntity);
			sessoes.add(pessoaFisicaEntity.getContatoEmail());
			sessoes.add(pessoaFisicaEntity.getEnderecoResidencial());
			sessoes.add(pessoaFisicaEntity.getEnderecoCorrespondencia());
			
			if (pessoaFisica.getContatoTelefoneComercial() != null) {
				sessoes.add(pessoaFisicaEntity.getContatoTelefoneComercial());
			}
			
			if (pessoaFisica.getContatoTelefoneMovel() != null) {
				sessoes.add(pessoaFisicaEntity.getContatoTelefoneMovel());
			}
			
			if (pessoaFisica.getContatoTelefoneResidencial() != null) {
				sessoes.add(pessoaFisicaEntity.getContatoTelefoneResidencial());
			}
			
			UsuarioEntity usuario = usuarioRepository.findById(2L).get();
			DominioEntity status = dominioRepository.findById(101L).get();
			LocalDate now = LocalDate.now();
			
			for (PopulaDadosDeSessao sessao : sessoes) {
				sessao.populaDadosDeSessao(usuario, status, now);
			}
			
			DominioEntity statusOcorrencia = dominioRepository.findById(10807L).get();
			OcorrenciaPessoaFisicaEntity ocorrencia = new OcorrenciaPessoaFisicaEntity();
			ocorrencia.setPessoa(pessoaFisicaEntity);
			ocorrencia.setStatus(statusOcorrencia);
			ocorrencia.setDataAtualizacao(pessoaFisicaEntity.getDataAtualizacao());
			ocorrencia.setUsuarioAtualizacao(usuario);
			logger.info("SALVANDO HISTORICO DE OCORRENCIA NO BANCO");
			ocorrencia = ocorrenciaRepository.save(ocorrencia);
			
			if (pessoaFisicaEntity.getFlagRegistroTecnico().equals('S') && pessoaFisica.getFlagRegistroTecnico() == false) {
				PageRequest page = PageRequest.of(0, 20);
				registroProfissionalService.deletarTodosRegistroConselhoProfissional(pessoaFisica.getCpf(), page);
				habilitacaoCadastroService.deletarTodosHabilitacaoCadastro(pessoaFisica.getCpf(), page);
			}
					
			logger.info("SALVANDO ATUALIZACAO DE DADOS DE PESSOA FISICA NO BANCO");
			pfRepository.save(pessoaFisicaEntity);
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage(), e);

		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}

	}
	
	public void ativaPessoaFisica(String cpf) throws Exception {
		logger.info("MUDANDO STATUS DE PESSOA FISICA PARA -> ATIVO");
		this.mudaEstado(cpf, 101L, 10805L);
	}

	public void cancelarPessoaFisica(String cpf) throws Exception {
		logger.info("MUDANDO STATUS DE PESSOA FISICA PARA -> CANCELADO");
		this.mudaEstado(cpf, 104L, 10803L);
	}

	public void suspendePessoaFisica(String cpf) throws Exception {
		logger.info("MUDANDO STATUS DE PESSOA FISICA PARA -> SUSPENSO");
		this.mudaEstado(cpf, 103L, 10804L);
	}

	public void mudaEstado(String cpf, Long codigo, Long codigoOcorrencia) throws Exception {
		try {
			logger.info("BUSCANDO PESSOA FISICA PARA ATUALIZAR ESTADO");
			PessoaFisicaEntity pessoaFisica = pfRepository.findByCpf(cpf).orElseThrow(() -> new ParametroNaoEncontradoException("PESSOA FISICA NÃO ENCONTRADA"));
			
			UsuarioEntity usuario = usuarioRepository.findById(2L).get();
			DominioEntity status = dominioRepository.findById(codigo).get();
			DominioEntity statusOcorrencia = dominioRepository.findById(codigoOcorrencia).get();
			pessoaFisica.setStatus(status);
			
			Pageable page = Pageable.unpaged();
			Page<PropiedadeAtividadeProdutivaProdutorEntity> listaAtividadeProdutiva = this.atividadeProdutivaProdutorRepository.findAllByPessoa(pessoaFisica.getId(), page);
			Page<VinculoRamoAtividadePessoaFisicaEntity> listaVinculoCNPJ = this.vinculoPJPFrepository.findAllByPessoa(pessoaFisica.getId(), page);
			Page<PessoaFisicaResponsavelTecnicoEntity> listaResponsavel = this.pessoaResponsavelRepository.findByIdPessoa(pessoaFisica.getId(), page);
			Page<PessoaFisicaHabilitacaoCredenciamentoEntity> listaHabilitacao =  this.pessoaHabilitacaoCredenciamentoRepository.findByIdPessoa(pessoaFisica.getId(), page);
			
			listaAtividadeProdutiva.map(atividade -> {
				atividade.setStatus(status);
				return atividade;
			});	
		
			listaVinculoCNPJ.map(VinculoCNPJ -> {
				VinculoCNPJ.setStatus(status);
				return VinculoCNPJ;
			});	
		
			listaResponsavel.map(Responsavel -> {
				Responsavel.setStatus(status);
				return Responsavel;
			});
		
			listaHabilitacao.map(Habilitacao -> {
				Habilitacao.setStatus(status);
				return Habilitacao ;
			});
	
			logger.info("SALVANDO MUDANÇAS DE ESTADO EM VINCULO DE ATIVIDADE PRODUTIVA RELACIONADO A PESSOA FISICA");
			this.atividadeProdutivaProdutorRepository.saveAll(listaAtividadeProdutiva);
			logger.info("SALVANDO MUDANÇAS DE ESTADO EM PESSOA JURIDICA RELACIONADA A PESSOA FISICA");
			this.vinculoPJPFrepository.saveAll(listaVinculoCNPJ);
			logger.info("SALVANDO MUDANÇAS DE ESTADO EM VINCULO DE RESPONSÁVEL TÉCNICO RELACIONADO A PESSOA FISICA");
			this.pessoaResponsavelRepository.saveAll(listaResponsavel);
			logger.info("SALVANDO MUDANÇAS DE ESTADO EM HABILITACÕES E CADASTROS RELACIONADOS A PESSOA FISICA");
			this.pessoaHabilitacaoCredenciamentoRepository.saveAll(listaHabilitacao);
				
			OcorrenciaPessoaFisicaEntity ocorrencia = new OcorrenciaPessoaFisicaEntity();
			ocorrencia.setPessoa(pessoaFisica);
			ocorrencia.setStatus(statusOcorrencia);
			ocorrencia.setDataAtualizacao(LocalDate.now());
			ocorrencia.setUsuarioAtualizacao(usuario);
			logger.info("SALVANDO HISTORICO DE OCORRENCIA NO BANCO");
			ocorrencia = ocorrenciaRepository.save(ocorrencia);
			

		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage(), e);

		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}
	}
	
	public void deletaPessoaFisica(String cpf) throws Exception {
		
		try {
			logger.info("BUSCANDO PESSOA FISICA NO BANCO");
			PessoaFisicaEntity pessoaFisica = pfRepository.findByCpf(cpf).orElseThrow(() -> new ParametroNaoEncontradoException("PESSOA FISICA NÃO ENCONTRADA"));
		
			PageRequest request = PageRequest.of(0, 20);
			Page<OcorrenciaPessoaFisicaEntity> ocorrencias = ocorrenciaRepository.findAllByCpf(pessoaFisica.getCpf(), request);
			
			ocorrencias.map(ocorrencia -> {
				ocorrenciaRepository.deleteById(ocorrencia.getId());
				return null;
			});
			
			logger.info("DELETANDO PESSOA FISICA DO BANCO");
			pfRepository.deleteById(pessoaFisica.getId());
			
		} catch (ParametroNaoEncontradoException e) {
			logger.error(e.getMessage());
			throw new ParametroNaoEncontradoException(e.getMessage(), e);

		} catch (Exception e) {
			logger.error("Erro inesperado");
			throw new Exception(e);
		}

	}

	public PessoaFisicaEntity converterParaEntity(
			PessoaFisicaEntity entity,
			PessoaFisicaCompletaVO vo
			) {
				
		entity.setCpf(vo.getCpf());
		entity.setRgRne(vo.getRgRne());
		entity.setDataEmissaoRG(vo.getDataEmissaoRG());
		entity.getContatoEmail().setDescricao(vo.getContatoEmail());
		entity.setNome(vo.getNome());
		entity.setOrgaoEmissor(vo.getOrgaoEmissor());
		entity.setTipoPessoa('F');
		
		entity.setFlagCaixaPostalCorr(SimNaoUtils.converteParaChar(vo.getFlagCaixaPostalCorr()));
		entity.setFlagMesmoEndRes(SimNaoUtils.converteParaChar(vo.getFlagCaixaPostalCorr()));
		entity.setFlagRecebeEmail(SimNaoUtils.converteParaChar(vo.getFlagCaixaPostalCorr()));
		entity.setFlagRecebeSms(SimNaoUtils.converteParaChar(vo.getFlagCaixaPostalCorr()));
		entity.setFlagRegistroTecnico(SimNaoUtils.converteParaChar(vo.getFlagCaixaPostalCorr()));
		
		entity.setObservacao(CheckObjectUtils.checkString(vo.getObservacoes()));
		entity.setNomeMae(CheckObjectUtils.checkString(vo.getNomeMae()));
		entity.setNomePai(CheckObjectUtils.checkString(vo.getNomeMae()));
		entity.setDataNascimento(CheckObjectUtils.checkLocalDate(vo.getDataNascimento()));
		
		entity.setEnderecoResidencial(new EnderecoEntity());
		entity.getEnderecoResidencial().setMunicipio(municipioRepository.findByNome(vo.getEnderecoResidencial().getMunicipio().getNome()));
		entity.setEnderecoResidencial(conversorEndereco.converterParaEnderecoEntity(entity.getEnderecoResidencial(), vo.getEnderecoResidencial()));
		
		entity.setOrgaoEmissorRg(dominioRepository.findByDominio(vo.getOrgaoEmissorRg()));
		entity.setUfOrgaoEmissorRg(ufRepository.findByCodUF(vo.getUfOrgaoEmissorRg()));
		entity.setEstadoCivil(dominioRepository.findByDominio(vo.getEstadoCivil()));
		entity.setSexo(dominioRepository.findByDominio(vo.getSexo()));
		entity.setNacionalidade(paisRepository.findByNome(vo.getNacionalidade()));
		entity.setMunicipioNaturalidade(municipioRepository.findByNome(vo.getMunicipioNaturalidade()));
		
		if (vo.getContatoTelefoneComercial() != null) {
			entity.setContatoTelefoneComercial(conversorTelefone.converterParaEntidade(new ContatoTelefoneEntity(), vo.getContatoTelefoneComercial()));
		}
		
		if (vo.getContatoTelefoneMovel() != null) {
			entity.setContatoTelefoneMovel(conversorTelefone.converterParaEntidade(new ContatoTelefoneEntity(), vo.getContatoTelefoneMovel()));
		}
		
		if (vo.getContatoTelefoneResidencial() != null) {
			entity.setContatoTelefoneResidencial(conversorTelefone.converterParaEntidade(new ContatoTelefoneEntity(), vo.getContatoTelefoneResidencial()));
		}
		
		if (vo.getFlagMesmoEndRes()) {
			entity.setEnderecoCorrespondencia(entity.getEnderecoResidencial());
		} else {
			entity.setEnderecoCorrespondencia(new EnderecoEntity());
			entity.getEnderecoCorrespondencia().setMunicipio(municipioRepository.findByNome(vo.getEnderecoCorrespondencia().getMunicipio().getNome()));
			entity.setEnderecoCorrespondencia(conversorEndereco.converterParaEnderecoEntity(entity.getEnderecoCorrespondencia(), vo.getEnderecoCorrespondencia()));
		}
		
		return entity;

	}

	public PessoaFisicaCompletaVO converterParaVOCompleto(PessoaFisicaEntity entity, PessoaFisicaCompletaVO vo) {
		
		vo.setCpf(entity.getCpf());
		vo.setRgRne(entity.getRgRne());
		vo.setNome(entity.getNome());
		vo.setOrgaoEmissor(entity.getOrgaoEmissor());
		vo.setDataEmissaoRG(entity.getDataEmissaoRG());
		
		vo.setOrgaoEmissorRg(entity.getOrgaoEmissorRg().getDominio());
		vo.setUfOrgaoEmissorRg(entity.getUfOrgaoEmissorRg().getCodigo());
		vo.setUfNaturalidade(entity.getUfOrgaoEmissorRg().getCodigo());
		vo.setEstadoCivil(entity.getEstadoCivil().getDominio());
		vo.setSexo(entity.getSexo().getDominio());
		vo.setNacionalidade(entity.getNacionalidade().getNome());
		
		if (entity.getMunicipioNaturalidade() != null) {
			vo.setMunicipioNaturalidade(entity.getMunicipioNaturalidade().getNome());
		}
		
		vo.setContatoEmail(entity.getContatoEmail().getDescricao());
		
		if (entity.getContatoTelefoneComercial() != null) {
			vo.setContatoTelefoneComercial(conversorTelefone.converterParaVO(entity.getContatoTelefoneComercial(), new ContatoTelefoneVO()));
		}
		
		if (entity.getContatoTelefoneMovel() != null) {
			vo.setContatoTelefoneMovel(conversorTelefone.converterParaVO(entity.getContatoTelefoneMovel(), new ContatoTelefoneVO()));
		}
		
		if (entity.getContatoTelefoneResidencial() != null) {
			vo.setContatoTelefoneResidencial(conversorTelefone.converterParaVO(entity.getContatoTelefoneResidencial(), new ContatoTelefoneVO()));
		}
		
		
		vo.setFlagCaixaPostalCorr(SimNaoUtils.converteParaBoolean(entity.getFlagCaixaPostalCorr()));
		vo.setFlagMesmoEndRes(SimNaoUtils.converteParaBoolean(entity.getFlagMesmoEndRes()));
		vo.setFlagRecebeEmail(SimNaoUtils.converteParaBoolean(entity.getFlagRecebeEmail()));
		vo.setFlagRecebeSms(SimNaoUtils.converteParaBoolean(entity.getFlagRecebeSms()));
		vo.setFlagRegistroTecnico(SimNaoUtils.converteParaBoolean(entity.getFlagRegistroTecnico()));
		
		vo.setNomeMae(CheckObjectUtils.checkString(entity.getNomeMae()));
		vo.setNomePai(CheckObjectUtils.checkString(entity.getNomeMae()));
		vo.setObservacoes(CheckObjectUtils.checkString(entity.getObservacao()));
		vo.setDataNascimento(CheckObjectUtils.checkLocalDate(entity.getDataNascimento()));
		
		vo.setEnderecoResidencial(conversorEndereco.converterParaEnderecoVO(entity.getEnderecoResidencial(), new EnderecoVO()));
		
		if (entity.getFlagMesmoEndRes().equals('S')) {
			vo.setEnderecoCorrespondencia(vo.getEnderecoResidencial());
		} else {
			vo.setEnderecoCorrespondencia(conversorEndereco.converterParaEnderecoVO(entity.getEnderecoCorrespondencia(), new EnderecoVO()));
		}
		
		return vo;
	} 
	
	public PessoaFisicaVO converterParaVO(PessoaFisicaEntity  entity, PessoaFisicaVO vo) {
		
		vo.setCpf(entity.getCpf());
		vo.setRg(entity.getRgRne());
		vo.setNome(entity.getNome());
		vo.setDataAlteracao(entity.getDataAtualizacao());
		vo.setSituacao(entity.getStatus().getDominio());
		
		if (entity.getMunicipioNaturalidade() != null) {
			vo.setMunicipio(entity.getMunicipioNaturalidade().getNome());
		}		
		vo.setCpfUsuarioAtualizacao(entity.getUsuarioAtualizacao().getPessoaFisica().getCpf());
		vo.setUsuarioAtualizacaoNome(entity.getUsuarioAtualizacao().getPessoaFisica().getNome());
		
		return vo;

	}
	
}
