package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.ApiarioResponsavelEntity;

@Repository
public interface ApiarioPessoaRepository extends  PagingAndSortingRepository<ApiarioResponsavelEntity, Long> {

	@Query("SELECT ap FROM ApiarioResponsavelEntity ap " + "WHERE ap.pessoa.id = ?1")
	Page<ApiarioResponsavelEntity> findByPessoa(Long id, Pageable page);

}
