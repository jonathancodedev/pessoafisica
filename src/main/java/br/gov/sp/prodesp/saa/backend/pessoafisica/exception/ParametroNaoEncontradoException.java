package br.gov.sp.prodesp.saa.backend.pessoafisica.exception;

public class ParametroNaoEncontradoException extends ServiceException {

	private static final long serialVersionUID = 1L;

	public ParametroNaoEncontradoException(String msg) {
		super(msg);
	}
	public ParametroNaoEncontradoException(String msg, Throwable tr) {
		super(msg, tr);
	}
}
