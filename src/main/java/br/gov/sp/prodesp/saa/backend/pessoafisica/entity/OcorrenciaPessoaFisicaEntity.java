package br.gov.sp.prodesp.saa.backend.pessoafisica.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity		   
@Table(name = "TB_CDA_PESSOA_FISICA_OCORRENCIA_REGISTRO")
public class OcorrenciaPessoaFisicaEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENT")
	private Long id;
	
	@Column(name = "DESCR_OCORRENCIA")
	private String descricao;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PESSOA_FISICA", referencedColumnName = "ID_PESSOA", nullable = false)
	private PessoaFisicaEntity pessoa;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "ID_DOM_TIPO_OCORRENCIA", referencedColumnName = "IDENT", nullable = false)
	private DominioEntity status;

    @Column(name = "DATA_ATUALIZ", nullable = true)
    private LocalDate dataAtualizacao;

    @JoinColumn(name = "ID_USUAR_ATUALIZ", referencedColumnName = "IDENT")
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private UsuarioEntity usuarioAtualizacao;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public PessoaFisicaEntity getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaFisicaEntity pessoa) {
		this.pessoa = pessoa;
	}

	public DominioEntity getStatus() {
		return status;
	}

	public void setStatus(DominioEntity status) {
		this.status = status;
	}

	public LocalDate getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDate dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public UsuarioEntity getUsuarioAtualizacao() {
		return usuarioAtualizacao;
	}

	public void setUsuarioAtualizacao(UsuarioEntity usuarioAtualizacao) {
		this.usuarioAtualizacao = usuarioAtualizacao;
	}
	
	

}
