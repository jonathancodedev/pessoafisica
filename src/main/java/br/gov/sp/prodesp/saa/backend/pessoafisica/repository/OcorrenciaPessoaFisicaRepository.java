package br.gov.sp.prodesp.saa.backend.pessoafisica.repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.OcorrenciaPessoaFisicaEntity;

@Repository
public interface OcorrenciaPessoaFisicaRepository extends PagingAndSortingRepository<OcorrenciaPessoaFisicaEntity, Long> {
	
	@Query("SELECT op FROM OcorrenciaPessoaFisicaEntity op WHERE op.pessoa.cpf = ?1")
	Page<OcorrenciaPessoaFisicaEntity> findAllByCpf(String cpf, Pageable page);

	@Transactional
	@Modifying
	@Query("DELETE FROM OcorrenciaPessoaFisicaEntity ha WHERE ha.id = ?1")
	void deleteById(Long id);
}
