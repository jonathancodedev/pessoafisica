package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.ApiarioPessoaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.OcorrenciaPessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaResponsavelTecnicoRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PropiedadeAtividadeProdutivaProdutorRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.UsuarioRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.VinculoPJPFRepository;

public abstract class AbstractControllerIT implements BaseControllerIT {
	
	@Autowired
	protected  TestRestTemplate restTemplate;

	@Autowired
	protected PessoaFisicaRepository pfRepository;
	
	@Autowired
	protected OcorrenciaPessoaFisicaRepository ocorrenciaRepository;
	
	@Autowired
	protected VinculoPJPFRepository vinculoPJPFrepository;

	@Autowired
	protected ApiarioPessoaRepository apiarioPessoaRepository;
	
	@Autowired
	protected PropiedadeAtividadeProdutivaProdutorRepository atividadeProdutivaProdutorRepository;

	@Autowired
	protected UsuarioRepository usuarioRepository;
	
	@Autowired
	protected PessoaFisicaResponsavelTecnicoRepository pessoaResponsavelRepository;

}