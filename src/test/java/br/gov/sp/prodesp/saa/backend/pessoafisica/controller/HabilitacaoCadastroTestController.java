package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaResponsavelTecnicoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaResponsavelTecnicoRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.service.HabilitacaoCadastroService;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaHabilitacaoCredenciamentoVO;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class HabilitacaoCadastroTestController extends AbstractControllerIT{

	@Autowired
	PessoaFisicaResponsavelTecnicoRepository pessoaResponsavelRepository;

	@Autowired
	HabilitacaoCadastroService habilitacaoCadastroService;

	private static StringBuilder path = new StringBuilder(URL);
	
	@BeforeEach
	void inicializar() {
		path.append("/habilitacao/");
	}
	
	@AfterEach
	void finalizar() {
		path = new StringBuilder(URL);
	}
	
	@Test
    @Order(0)    
	void esperaBuscarHabilitacaoCadastroPorCPF() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Pageable pg = Pageable.ofSize(1);
		PessoaFisicaResponsavelTecnicoEntity pessoa = pessoaResponsavelRepository.findAll(pg).stream().findAny().get();
		path.append(pessoa.getPessoaFisica().getCpf());
		
		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);
		
		assertEquals(true, response.hasBody());
	}
	
	@Test
    @Order(1)    
	void esperaIncluirHabilitacaoCadastro() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>(){};
		
		Page<PessoaFisicaHabilitacaoCredenciamentoVO> pagePessoaVO = habilitacaoCadastroService.buscaHabilitacaoCadastro("00542346893", null);
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = pagePessoaVO.stream().findFirst().get();
		pessoaVO.setCpf("52095389077");
		pessoaVO.setTipoHabilitacao("Cadastro");
		pessoaVO.setHabilitacaoCredenciamento("Emissão de GTA de EGRESSO DE EVENTOS DE CONCENTRAÇÃO");
		
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(2)    
	void esperaAtualizarHabilitacaoCadastro() throws Exception{
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		
		Page<PessoaFisicaHabilitacaoCredenciamentoVO> pagePessoaVO = habilitacaoCadastroService.buscaHabilitacaoCadastro("52095389077", null);
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = pagePessoaVO.stream().findFirst().get();
		
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(3)    
	void esperaDeletarHabilitacaoCadastro() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Page<PessoaFisicaHabilitacaoCredenciamentoVO> pagePessoaVO = habilitacaoCadastroService.buscaHabilitacaoCadastro("52095389077", null);
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = pagePessoaVO.stream().findAny().get();

		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.DELETE, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(4)    
	void esperaPorParametroNaoEncontradoExceptionNaBuscaPorHabilitacaoCadastroPorCpf() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		path.append("242252353535876546789076546789");
		
		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

	}
	
	
	@Test
    @Order(5)    
	void esperaPorParametroInvalidoExceptionSalvandoNovaHabilitacaoCadastro() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>(){};
		
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = new PessoaFisicaHabilitacaoCredenciamentoVO();
		pessoaVO.setCpf("5209538907456787");
		
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
    @Order(6)    
	void esperaPorDadosJaExistentesExceptionSalvandoNovaHabilitacaoCadastro() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>(){};
		
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = new PessoaFisicaHabilitacaoCredenciamentoVO();
		pessoaVO.setCpf("00188561790");
		pessoaVO.setHabilitacaoCredenciamento("Emissão de GTA de EGRESSO DE EVENTOS DE CONCENTRAÇÃO");
		
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
    @Order(7)    
	void esperaParametroInvalidoExceptionAtualizandoHabilitacaoCadastro() throws Exception{
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = new PessoaFisicaHabilitacaoCredenciamentoVO();
		pessoaVO.setCpf("1111");	
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
    @Order(8)    
	void esperaParametroNaoEncontradoExceptionAtualizandoHabilitacaoCadastro() throws Exception{
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = new PessoaFisicaHabilitacaoCredenciamentoVO();
		pessoaVO.setCpf("00542346821");
		
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
    @Order(9)    
	void esperaParametroNaoEncontradoExceptionAoDeletarHabilitacaoCadastro() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaHabilitacaoCredenciamentoVO pessoaVO = new PessoaFisicaHabilitacaoCredenciamentoVO();
		pessoaVO.setCpf("00542346821");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO> entity = new HttpEntity<PessoaFisicaHabilitacaoCredenciamentoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.DELETE, entity, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	
}
