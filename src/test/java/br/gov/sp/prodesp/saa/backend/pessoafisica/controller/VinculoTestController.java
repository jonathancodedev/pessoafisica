package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoApiarioVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoAtividadeProdutivaVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.VinculoCNPJVO;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class VinculoTestController extends AbstractControllerIT {

	private static StringBuilder path = new StringBuilder(URL);
	
	@BeforeEach
	void inicializar() {
		path.append("/vinculo/");
	}
	
	@AfterEach
	void finalizar() {
		path = new StringBuilder(URL);
	}
	
	@Test
	void esperaEncontrarVinculoCNPJ() {
		
		path.append("cnpj/" + "84768304834");
		ParameterizedTypeReference<VinculoCNPJVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<VinculoCNPJVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void esperaEncontrarVinculoApiario() {

		path.append("apiario/" + "12497246866" );
		ParameterizedTypeReference<VinculoApiarioVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<VinculoApiarioVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void esperaEncontrarVinculoAtividadeProdutiva() {

		path.append("atividade/" + "22677926806" );
		ParameterizedTypeReference<VinculoAtividadeProdutivaVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<VinculoAtividadeProdutivaVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	void esperaPorParametroNaoEncontradoDeVinculoCNPJ() {
		
		path.append("cnpj/" + "84648102834");
		ParameterizedTypeReference<VinculoCNPJVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<VinculoCNPJVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	void esperaPorParametroNaoEncontradoDeVinculoApiario() {

		path.append("apiario/" + "8468307934" );
		ParameterizedTypeReference<VinculoApiarioVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<VinculoApiarioVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	void esperaPorParametroNaoEncontradoDeVinculoAtividadeProdutiva() {

		path.append("atividade/" + "84648302834" );
		ParameterizedTypeReference<VinculoAtividadeProdutivaVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<VinculoAtividadeProdutivaVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
}
