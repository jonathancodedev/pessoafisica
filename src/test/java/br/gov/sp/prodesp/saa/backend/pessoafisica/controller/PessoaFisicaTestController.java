package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.service.PessoaFisicaService;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaCompletaVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaVO;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class PessoaFisicaTestController extends AbstractControllerIT{
	
	@Autowired
	PessoaFisicaService pessoaFisicaService;

	private static StringBuilder path = new StringBuilder(URL);
	
	@BeforeEach
	void inicializar() {
		path.append("/pessoafisica/");
	}
	
	@AfterEach
	void finalizar() {
		path = new StringBuilder(URL);
	}
	
	public String COMPLETO = "visualiza/"; 
	public String ATIVA = "ativar/"; 
	public String CANCELA = "cancelar/"; 
	public String SUSPENDE = "suspender/"; 


	
	@Test
    @Order(0)    
	void esperaEncontrarDadosdePessoaFisicaPorCPF() {
		Pageable pg = Pageable.ofSize(1);
		PessoaFisicaEntity ent = pfRepository.findAll(pg).stream().findAny().get();
		path.append(ent.getCpf());
		ParameterizedTypeReference<PessoaFisicaVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<PessoaFisicaVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(ent.getCpf(), response.getBody().getCpf());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(1)    
	void esperaVisualizarDadosPessoaFisicaPorCPF() {
		Pageable pg = Pageable.ofSize(1);
		PessoaFisicaEntity ent = pfRepository.findAll(pg).stream().findAny().get();
		path.append(COMPLETO + ent.getCpf().toString());
		ParameterizedTypeReference<PessoaFisicaCompletaVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<PessoaFisicaCompletaVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(ent.getNome(), response.getBody().getNome());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(2)    
	void esperaSalvarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = pessoaFisicaService.buscaPessoaFisicaCompletaPorCPF("02342668058");
		pessoaVO.setCpf("48725794027");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaCompletaVO> entity = new HttpEntity<PessoaFisicaCompletaVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
	}
	
	
	@Test
    @Order(3)    
	void esperaEditarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = pessoaFisicaService.buscaPessoaFisicaCompletaPorCPF("48725794027");
		pessoaVO.setNome("ATUALIZADO");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaCompletaVO> entity = new HttpEntity<PessoaFisicaCompletaVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
    @Order(4)    
	void ativarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = pessoaFisicaService.buscaPessoaFisicaCompletaPorCPF("48725794027");
		path.append(ATIVA + pessoaVO.getCpf());

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, null, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(5)    
	void cancelarPessoaFisica() throws Exception  {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = pessoaFisicaService.buscaPessoaFisicaCompletaPorCPF("48725794027");
		path.append(CANCELA + pessoaVO.getCpf());

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, null, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(6)    
	void suspenderPessoaFisica() throws Exception  {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = pessoaFisicaService.buscaPessoaFisicaCompletaPorCPF("48725794027");
		path.append(SUSPENDE + pessoaVO.getCpf());

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, null, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(7)    
	void esperaDeletarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = pessoaFisicaService.buscaPessoaFisicaCompletaPorCPF("48725794027");
		path.append(pessoaVO.getCpf());

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.DELETE, null, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
    @Order(8)    
	void esperaPorParametroNaoEncontradoBuscandoDadosBasicosDePessoaFisicaPorCPF() {
		
		path.append("00542342221");
		ParameterizedTypeReference<PessoaFisicaVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<PessoaFisicaVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
    @Order(9)    
	void esperaPorParametroNaoEncontradoBuscandoDadosCompletosDePessoaFisicaPorCPF() {
		path.append(COMPLETO + "00542342221");
		ParameterizedTypeReference<PessoaFisicaCompletaVO> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<PessoaFisicaCompletaVO> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
    @Order(10)    
	void esperaPorParametroInvalidoAoSalvarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = new PessoaFisicaCompletaVO();
		pessoaVO.setCpf("487257940271212122");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaCompletaVO> entity = new HttpEntity<PessoaFisicaCompletaVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		
	}
	
	@Test
    @Order(11)    
	void esperaPorDadosJaCadastradosAoSalvarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = new PessoaFisicaCompletaVO();
		pessoaVO.setCpf("01054102848");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaCompletaVO> entity = new HttpEntity<PessoaFisicaCompletaVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		
	}
	
	
	@Test
    @Order(12)    
	void esperaPorParametroInvalidoAoEditarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = new PessoaFisicaCompletaVO();
		pessoaVO.setCpf("487257940271212122");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaCompletaVO> entity = new HttpEntity<PessoaFisicaCompletaVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
    @Order(13)    
	void esperaPorParametroNaoEncontradoAoEditarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaCompletaVO pessoaVO = new PessoaFisicaCompletaVO();
		pessoaVO.setCpf("11344102848");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaCompletaVO> entity = new HttpEntity<PessoaFisicaCompletaVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
    @Order(14)    
	void esperaPorParametroNaoEncontradoAoAtivarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		path.append(ATIVA + "11344102848");

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, null, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
    @Order(15)    
	void esperaPorParametroNaoEncontradoAoCancelarPessoaFisica() throws Exception  {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		path.append(CANCELA + "11344102848");

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, null, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
    @Order(16)    
	void esperaPorParametroNaoEncontradoAoSuspenderPessoaFisica() throws Exception  {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		path.append(SUSPENDE + "11344102848");

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, null, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
    @Order(17)    
	void esperaPorParametroNaoEncontradoAoDeletarPessoaFisica() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		path.append("11344102848");

		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.DELETE, null, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	

}

