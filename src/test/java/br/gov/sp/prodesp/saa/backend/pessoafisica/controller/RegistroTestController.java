package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaResponsavelTecnicoEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.service.RegistroProfissionalService;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.DeletarDadosRegistroVO;
import br.gov.sp.prodesp.saa.backend.pessoafisica.vo.PessoaFisicaResponsavelTecnicoVO;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class RegistroTestController extends AbstractControllerIT {
	
	@Autowired
	RegistroProfissionalService registroProfissionalService;
	
	private static StringBuilder path = new StringBuilder(URL);
	
	@BeforeEach
	void inicializar() {
		path.append("/registro/");
	}
	
	@AfterEach
	void finalizar() {
		path = new StringBuilder(URL);
	}

	@Test
	@Order(1)
	void esperaBuscarRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Pageable pg = Pageable.ofSize(1);
		PessoaFisicaResponsavelTecnicoEntity ent = pessoaResponsavelRepository.findAll(pg).stream().findAny().get();
		path.append(ent.getPessoaFisica().getCpf());
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	@Order(2)
	void esperaIncluirRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Pageable pg = Pageable.ofSize(1);
		Page<PessoaFisicaResponsavelTecnicoVO> pessoa = registroProfissionalService.buscaRegistroConselhoProfisssional("12174312840", pg);
		PessoaFisicaResponsavelTecnicoVO pessoaVO = pessoa.stream().findAny().get();
		pessoaVO.setFormacao("Técnico Agrícola");
		pessoaVO.setnRegistroEstadual("50601664933");
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaResponsavelTecnicoVO> entity = new HttpEntity<PessoaFisicaResponsavelTecnicoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	@Order(3)
	void esperaAtualizarRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Pageable pg = Pageable.ofSize(1);
		Page<PessoaFisicaResponsavelTecnicoVO> pessoa = registroProfissionalService.buscaRegistroConselhoProfisssional("12174312840", pg);
		PessoaFisicaResponsavelTecnicoVO pessoaVO = pessoa.stream().findAny().get();
		pessoaVO.setConselhoProfissional("CREA - Conselho Regional de Engenharia e Agronomia");
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaResponsavelTecnicoVO> entity = new HttpEntity<PessoaFisicaResponsavelTecnicoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	@Order(4)
	void esperaDeletarRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		DeletarDadosRegistroVO dados = new DeletarDadosRegistroVO();
		dados.setCpf("12174312840");
		dados.setFormacaoAcademica("Técnico Agrícola");

		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<DeletarDadosRegistroVO> entity = new HttpEntity<DeletarDadosRegistroVO>(dados, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.DELETE, entity, responseType);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		
	}
	

	@Test
	@Order(5)
	void esperaPorParametroNaoEncontraoAoBuscarRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		path.append("00542342221");
		
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
	@Order(6)
	void esperaPorParametroInvalidoAoTentarIncluirRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaResponsavelTecnicoVO pessoaVO = new PessoaFisicaResponsavelTecnicoVO();
		pessoaVO.setCpf("00544444443322221");
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaResponsavelTecnicoVO> entity = new HttpEntity<PessoaFisicaResponsavelTecnicoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	@Order(7)
	void esperaPorDadosJaCadastradosAoTentarIncluirRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaResponsavelTecnicoVO pessoaVO = new PessoaFisicaResponsavelTecnicoVO();
		pessoaVO.setCpf("01546267832");
		pessoaVO.setFormacao("Medicina Veterinária");
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaResponsavelTecnicoVO> entity = new HttpEntity<PessoaFisicaResponsavelTecnicoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.POST, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	@Order(8)
	void esperaPorParametroInvalidoAoAtualizarRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaResponsavelTecnicoVO pessoaVO = new PessoaFisicaResponsavelTecnicoVO();
		pessoaVO.setCpf("00544444443322221");
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaResponsavelTecnicoVO> entity = new HttpEntity<PessoaFisicaResponsavelTecnicoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	@Order(9)
	void esperaPorParametroNaoEncontradoAoAtualizarRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		PessoaFisicaResponsavelTecnicoVO pessoaVO = new PessoaFisicaResponsavelTecnicoVO();
		pessoaVO.setCpf("00542342221");
		pessoaVO.setFormacao("Dummy");
		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<PessoaFisicaResponsavelTecnicoVO> entity = new HttpEntity<PessoaFisicaResponsavelTecnicoVO>(pessoaVO, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.PUT, entity, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
	@Order(10)
	void esperaPorParametroNaoEncontradoAoDeletarRegistroProfissional() throws Exception {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		DeletarDadosRegistroVO dados = new DeletarDadosRegistroVO();
		dados.setCpf("00542342221");
		dados.setFormacaoAcademica("Dummy");

		ParameterizedTypeReference<PessoaFisicaEntity> responseType = new ParameterizedTypeReference<>() {};
		HttpEntity<DeletarDadosRegistroVO> entity = new HttpEntity<DeletarDadosRegistroVO>(dados, headers);
		ResponseEntity<PessoaFisicaEntity> response = restTemplate.exchange(path.toString(), HttpMethod.DELETE, entity, responseType);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		
	}
}
