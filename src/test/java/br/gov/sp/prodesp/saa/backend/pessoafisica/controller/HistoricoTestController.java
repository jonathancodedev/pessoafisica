package br.gov.sp.prodesp.saa.backend.pessoafisica.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.gov.sp.prodesp.saa.backend.pessoafisica.entity.PessoaFisicaEntity;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.OcorrenciaPessoaFisicaRepository;
import br.gov.sp.prodesp.saa.backend.pessoafisica.repository.PessoaFisicaRepository;

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class HistoricoTestController extends AbstractControllerIT{

	@Autowired
	PessoaFisicaRepository pessoaFisicaRepository;
	
	@Autowired
	OcorrenciaPessoaFisicaRepository ocorrenciaRepository;

	private static StringBuilder path = new StringBuilder(URL);
	
	@BeforeEach
	void inicializar() {
		path.append("/historico/");
	}
	
	@AfterEach
	void finalizar() {
		path = new StringBuilder(URL);
	}
	
	@Test
	void esperaEncontrarHistoricodePessoaFisica() throws Exception {
		
		PageRequest request = PageRequest.of(0, 1);
		PessoaFisicaEntity ent = pfRepository.findAll(request).stream().findAny().get();
		path.append(ent.getCpf());
		
		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);
		

		assertEquals(response.hasBody(), true);
	}
	
	@Test
	void esperaParametroNaoEncontradoExceptionHistoricodePessoaFisica() throws Exception {
		
		path.append("00542346821");
		
		ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<>() {
		};
		ResponseEntity<Object> response = restTemplate.exchange(path.toString(), HttpMethod.GET, null, responseType);
		

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
}
